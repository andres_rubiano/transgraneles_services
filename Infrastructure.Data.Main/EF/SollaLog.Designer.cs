﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Data.EntityClient;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Runtime.Serialization;

[assembly: EdmSchemaAttribute()]

namespace Infrastructure.Data.Main.EF
{
    #region Contexts
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    public partial class SollaLogEntities : ObjectContext
    {
        #region Constructors
    
        /// <summary>
        /// Initializes a new SollaLogEntities object using the connection string found in the 'SollaLogEntities' section of the application configuration file.
        /// </summary>
        public SollaLogEntities() : base("name=SollaLogEntities", "SollaLogEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new SollaLogEntities object.
        /// </summary>
        public SollaLogEntities(string connectionString) : base(connectionString, "SollaLogEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new SollaLogEntities object.
        /// </summary>
        public SollaLogEntities(EntityConnection connection) : base(connection, "SollaLogEntities")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        #endregion
    
        #region Partial Methods
    
        partial void OnContextCreated();
    
        #endregion
    
        #region ObjectSet Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        public ObjectSet<SoapCallLog> SoapCallLog
        {
            get
            {
                if ((_SoapCallLog == null))
                {
                    _SoapCallLog = base.CreateObjectSet<SoapCallLog>("SoapCallLog");
                }
                return _SoapCallLog;
            }
        }
        private ObjectSet<SoapCallLog> _SoapCallLog;

        #endregion
        #region AddTo Methods
    
        /// <summary>
        /// Deprecated Method for adding a new object to the SoapCallLog EntitySet. Consider using the .Add method of the associated ObjectSet&lt;T&gt; property instead.
        /// </summary>
        public void AddToSoapCallLog(SoapCallLog soapCallLog)
        {
            base.AddObject("SoapCallLog", soapCallLog);
        }

        #endregion
    }
    

    #endregion
    
    #region Entities
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    [EdmEntityTypeAttribute(NamespaceName="SollaLogModel", Name="SoapCallLog")]
    [Serializable()]
    [DataContractAttribute(IsReference=true)]
    public partial class SoapCallLog : EntityObject
    {
        #region Factory Method
    
        /// <summary>
        /// Create a new SoapCallLog object.
        /// </summary>
        /// <param name="id">Initial value of the Id property.</param>
        public static SoapCallLog CreateSoapCallLog(global::System.Int32 id)
        {
            SoapCallLog soapCallLog = new SoapCallLog();
            soapCallLog.Id = id;
            return soapCallLog;
        }

        #endregion
        #region Primitive Properties
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=true, IsNullable=false)]
        [DataMemberAttribute()]
        public global::System.Int32 Id
        {
            get
            {
                return _Id;
            }
            set
            {
                if (_Id != value)
                {
                    OnIdChanging(value);
                    ReportPropertyChanging("Id");
                    _Id = StructuralObject.SetValidValue(value);
                    ReportPropertyChanged("Id");
                    OnIdChanged();
                }
            }
        }
        private global::System.Int32 _Id;
        partial void OnIdChanging(global::System.Int32 value);
        partial void OnIdChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String OperationName
        {
            get
            {
                return _OperationName;
            }
            set
            {
                OnOperationNameChanging(value);
                ReportPropertyChanging("OperationName");
                _OperationName = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("OperationName");
                OnOperationNameChanged();
            }
        }
        private global::System.String _OperationName;
        partial void OnOperationNameChanging(global::System.String value);
        partial void OnOperationNameChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String Request
        {
            get
            {
                return _Request;
            }
            set
            {
                OnRequestChanging(value);
                ReportPropertyChanging("Request");
                _Request = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("Request");
                OnRequestChanged();
            }
        }
        private global::System.String _Request;
        partial void OnRequestChanging(global::System.String value);
        partial void OnRequestChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public global::System.String Response
        {
            get
            {
                return _Response;
            }
            set
            {
                OnResponseChanging(value);
                ReportPropertyChanging("Response");
                _Response = StructuralObject.SetValidValue(value, true);
                ReportPropertyChanged("Response");
                OnResponseChanged();
            }
        }
        private global::System.String _Response;
        partial void OnResponseChanging(global::System.String value);
        partial void OnResponseChanged();
    
        /// <summary>
        /// No Metadata Documentation available.
        /// </summary>
        [EdmScalarPropertyAttribute(EntityKeyProperty=false, IsNullable=true)]
        [DataMemberAttribute()]
        public Nullable<global::System.DateTime> CallDateTime
        {
            get
            {
                return _CallDateTime;
            }
            set
            {
                OnCallDateTimeChanging(value);
                ReportPropertyChanging("CallDateTime");
                _CallDateTime = StructuralObject.SetValidValue(value);
                ReportPropertyChanged("CallDateTime");
                OnCallDateTimeChanged();
            }
        }
        private Nullable<global::System.DateTime> _CallDateTime;
        partial void OnCallDateTimeChanging(Nullable<global::System.DateTime> value);
        partial void OnCallDateTimeChanged();

        #endregion
    
    }

    #endregion
    
}
