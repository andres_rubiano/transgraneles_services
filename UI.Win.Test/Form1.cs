﻿using System;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDFView;
using PDFView.ConvertPDF;
using Simplexity.Email;
using Simplexity.Solla.BusinessRules.Main.Services;
using Simplexity.Solla.Common.Constants;
using UI.Win.Test.SollaService;

namespace UI.Win.Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
          try
          {
            var messageService = new MessagesService();
            messageService.ProcessPendingMessages();
          }
          catch (Exception ex)
          {

            ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
          }
          

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
         
            var prueba = new MessagesService();
            string pFaxNumber = "1-4222424";
            string numero;
            //numero = prueba.CreateFaxNumber(pFaxNumber);
        }

        private void button3_Click(object sender, EventArgs e)
        {

          try
          {
            var cartera = new ZWS_SALDO_CARTERA();

            var networkCredential = new System.Net.NetworkCredential("HAROJAS", "Audi2010*");
              cartera.Credentials = networkCredential;

            decimal valorCartera;
            int tipoMensaje;

              var message = cartera.ZfiWsCartera("8909002918", "7000", out valorCartera, out tipoMensaje);

          }
          catch (Exception ex)
          {

            var mensaje = ";";
          }

        }

        private void btnEmail_Click(object sender, EventArgs e)
        {
          Email mail = new Email("EmailSettings");
          mail.SendMail(txtTo.Text, "test", "prueba");
        }

        private void btnFormatFaxNumber_Click(object sender, EventArgs e)
        {
          txtFormattedFaxNumber.Text = string.Empty;
          lblMessage.Text = string.Empty;

          var messagesService = new MessagesService();

          var messageDTO = messagesService.CreateFaxNumber(txtNoFormattedFaxNumber.Text);

          if (messageDTO.TypeEnum != MessagesConstants.Types.Information)
          {
            lblMessage.Text = messageDTO.Message;
          }
          else
          {
            txtFormattedFaxNumber.Text = messageDTO.TransactionNumber;
          }
        }

        private void button4_Click(object sender, EventArgs e)
        {
          //SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();
          //f.OpenPdf(@"d:\solla\test.pdf");

          //if (f.PageCount > 0)
          //{
          //  //Save to multipage TIFF file with 300 dpi
          //  f.ImageOptions.Dpi = 300;
          //  f.ToMultipageTiff(@"d:\solla\test.tif");
          //}

        }

        private void button5_Click(object sender, EventArgs e)
        {
          var COLOR_TIFF_RGB = "tiff24nc";

          var returnFileName = PDFConvert.ConvertPdfToGraphic(@"d:\solla\test.pdf", @"d:\solla\test.tif", COLOR_TIFF_RGB, 150);

          try
          {
            ImageUtil.CompressTiff(returnFileName);
          }
          catch (Exception)
          {
            MessageBox.Show(
              "An error occurred while applying LZW compression to the TIFF file. The TIFF file has been saved in an uncompressed format instead.",
              "TIFF Compression Error",
              MessageBoxButtons.OK);
          }
        }
        
    }
}
