﻿namespace UI.Win.Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.button1 = new System.Windows.Forms.Button();
      this.button2 = new System.Windows.Forms.Button();
      this.button3 = new System.Windows.Forms.Button();
      this.btnEmail = new System.Windows.Forms.Button();
      this.txtTo = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.btnFormatFaxNumber = new System.Windows.Forms.Button();
      this.txtNoFormattedFaxNumber = new System.Windows.Forms.TextBox();
      this.txtFormattedFaxNumber = new System.Windows.Forms.TextBox();
      this.lblMessage = new System.Windows.Forms.Label();
      this.button4 = new System.Windows.Forms.Button();
      this.button5 = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(55, 42);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(154, 23);
      this.button1.TabIndex = 0;
      this.button1.Text = "processMesages";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // button2
      // 
      this.button2.Location = new System.Drawing.Point(55, 71);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(154, 23);
      this.button2.TabIndex = 1;
      this.button2.Text = "test fax n umbers";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new System.EventHandler(this.button2_Click);
      // 
      // button3
      // 
      this.button3.Location = new System.Drawing.Point(55, 100);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(154, 23);
      this.button3.TabIndex = 2;
      this.button3.Text = "test cartera service";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new System.EventHandler(this.button3_Click);
      // 
      // btnEmail
      // 
      this.btnEmail.Location = new System.Drawing.Point(21, 201);
      this.btnEmail.Name = "btnEmail";
      this.btnEmail.Size = new System.Drawing.Size(154, 23);
      this.btnEmail.TabIndex = 3;
      this.btnEmail.Text = "test email";
      this.btnEmail.UseVisualStyleBackColor = true;
      this.btnEmail.Click += new System.EventHandler(this.btnEmail_Click);
      // 
      // txtTo
      // 
      this.txtTo.Location = new System.Drawing.Point(48, 175);
      this.txtTo.Name = "txtTo";
      this.txtTo.Size = new System.Drawing.Size(127, 20);
      this.txtTo.TabIndex = 4;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(13, 175);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(19, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "to:";
      // 
      // btnFormatFaxNumber
      // 
      this.btnFormatFaxNumber.Location = new System.Drawing.Point(295, 201);
      this.btnFormatFaxNumber.Name = "btnFormatFaxNumber";
      this.btnFormatFaxNumber.Size = new System.Drawing.Size(154, 23);
      this.btnFormatFaxNumber.TabIndex = 6;
      this.btnFormatFaxNumber.Text = "test CreateFaxNumber()";
      this.btnFormatFaxNumber.UseVisualStyleBackColor = true;
      this.btnFormatFaxNumber.Click += new System.EventHandler(this.btnFormatFaxNumber_Click);
      // 
      // txtNoFormattedFaxNumber
      // 
      this.txtNoFormattedFaxNumber.Location = new System.Drawing.Point(310, 42);
      this.txtNoFormattedFaxNumber.Name = "txtNoFormattedFaxNumber";
      this.txtNoFormattedFaxNumber.Size = new System.Drawing.Size(100, 20);
      this.txtNoFormattedFaxNumber.TabIndex = 7;
      // 
      // txtFormattedFaxNumber
      // 
      this.txtFormattedFaxNumber.Location = new System.Drawing.Point(310, 71);
      this.txtFormattedFaxNumber.Name = "txtFormattedFaxNumber";
      this.txtFormattedFaxNumber.Size = new System.Drawing.Size(100, 20);
      this.txtFormattedFaxNumber.TabIndex = 8;
      // 
      // lblMessage
      // 
      this.lblMessage.AutoSize = true;
      this.lblMessage.Location = new System.Drawing.Point(310, 109);
      this.lblMessage.Name = "lblMessage";
      this.lblMessage.Size = new System.Drawing.Size(60, 13);
      this.lblMessage.TabIndex = 9;
      this.lblMessage.Text = "lblMessage";
      // 
      // button4
      // 
      this.button4.Location = new System.Drawing.Point(21, 253);
      this.button4.Name = "button4";
      this.button4.Size = new System.Drawing.Size(154, 23);
      this.button4.TabIndex = 10;
      this.button4.Text = "convert PDF to TIFF";
      this.button4.UseVisualStyleBackColor = true;
      this.button4.Click += new System.EventHandler(this.button4_Click);
      // 
      // button5
      // 
      this.button5.Location = new System.Drawing.Point(199, 253);
      this.button5.Name = "button5";
      this.button5.Size = new System.Drawing.Size(154, 23);
      this.button5.TabIndex = 11;
      this.button5.Text = "convert PDF to TIFF";
      this.button5.UseVisualStyleBackColor = true;
      this.button5.Click += new System.EventHandler(this.button5_Click);
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(522, 371);
      this.Controls.Add(this.button5);
      this.Controls.Add(this.button4);
      this.Controls.Add(this.lblMessage);
      this.Controls.Add(this.txtFormattedFaxNumber);
      this.Controls.Add(this.txtNoFormattedFaxNumber);
      this.Controls.Add(this.btnFormatFaxNumber);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.txtTo);
      this.Controls.Add(this.btnEmail);
      this.Controls.Add(this.button3);
      this.Controls.Add(this.button2);
      this.Controls.Add(this.button1);
      this.Name = "Form1";
      this.Text = "Form1";
      this.Load += new System.EventHandler(this.Form1_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnEmail;
        private System.Windows.Forms.TextBox txtTo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnFormatFaxNumber;
        private System.Windows.Forms.TextBox txtNoFormattedFaxNumber;
        private System.Windows.Forms.TextBox txtFormattedFaxNumber;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
    }
}

