﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using System.Threading;
using Telerik.Windows.Controls;
using UI.ControlTrafico.Mapa.TraficoService;
using UI.ControlTrafico.UserControls;
using Telerik.Windows.Controls.Map;
using SelectionChangedEventArgs = System.Windows.Controls.SelectionChangedEventArgs;

namespace UI.ControlTrafico.Mapa
  {
    public partial class MainPage : UserControl
      {
        private bool _termino = false;
        public string Sucursal { get; set; }
        public string Empresa { get; set; }
        public string Cliente { get; set; }
        private TraficoService.TraficoClient _wsc;
        private VehiclesGpsData vehiclesGpsSelected;


        public MainPage()
          {
            InitializeComponent();
            _wsc = new TraficoClient();
            _wsc.RequestVehicleGpsInfoCompleted += new EventHandler<RequestVehicleGpsInfoCompletedEventArgs>(WscRequestVehicleGpsInfoCompleted);
            _wsc.RequestVehicleGpsInfoWithoutTierCompleted += _wsc_RequestVehicleGpsInfoWithoutTierCompleted;
            //Sucursal = cookiesocid;
            //Empresa = cookiesoccompid;
            //Cliente = cookieTieCode;

            this.Loaded += MainPage_Loaded;
          }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
          {
            var c = Cliente;
            if (string.IsNullOrEmpty(Empresa))
              {
                return;
              }

            simplexityVehicleMap1.M00StartAppMap();
            CargarUnidades();
            this.Unloaded += new RoutedEventHandler(MainPageUnloaded);
            var myDispatcherTimer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 0, 0, 100000)};
            myDispatcherTimer.Tick += new EventHandler(Cargar);
            myDispatcherTimer.Start();
          }

        private void _wsc_RequestVehicleGpsInfoWithoutTierCompleted(object sender, RequestVehicleGpsInfoWithoutTierCompletedEventArgs e)
          {
            var t = e.Result.ToList();
            FillData(t, true);
          }

        public void Cargar(object s, EventArgs e)
          {
            CargarUnidades();
          }

        public void MainPageUnloaded(object sender, RoutedEventArgs e)
          {
            _termino = true;
          }

        public void CargarUnidades()
          {
            if (string.IsNullOrEmpty(Cliente))
              {
                _wsc.RequestVehicleGpsInfoWithoutTierAsync(Sucursal, Empresa);
              }
            else
              {
                _wsc.RequestVehicleGpsInfoAsync(Sucursal, Empresa, Cliente);
              }
          }

        public void FillData(List<VehiclesGpsData> listVehicles, bool listRefresh)
          {
            List<VehiclesGpsData> listVehiclesGps = listVehicles;

            if (listRefresh)
              {
                lstVehiculos.ItemsSource = listVehiclesGps;
                lstVehiculos.DisplayMemberPath = "VehCode";
              }

            foreach (var selectVehicle in listVehiclesGps)
              {
                if (selectVehicle.Deltac > 0 &&
                    selectVehicle.Deltac < 30)
                  {
                    simplexityVehicleMap1.M02AddVehicle(selectVehicle.VehCode, selectVehicle.Latitude, selectVehicle.Longitude,
                                                        Color.FromArgb(248, 248, 232, 23), "Atraso 30 min",
                                                        "Registro :\n" +
                                                        selectVehicle.GpsDate +
                                                        "\nVel: " + selectVehicle.Speed.ToString() + " Km/h" +
                                                        "\nRumbo: " + selectVehicle.Direction.ToString() + " °N" +
                                                        "\nReferencia:" + selectVehicle.GeoRef,
                                                        SimplexityVehicleMap.Imagenes.CamionNaranja3);
                  }
                if (selectVehicle.Deltac >= 30 &&
                    selectVehicle.Deltac < 60)
                  {
                    simplexityVehicleMap1.M02AddVehicle(selectVehicle.VehCode, selectVehicle.Latitude, selectVehicle.Longitude,
                                                        Color.FromArgb(248, 248, 145, 23), "Atraso 30 a 60 min",
                                                        "Registro :\n" +
                                                        selectVehicle.GpsDate +
                                                        "\nVel: " + selectVehicle.Speed.ToString() + " Km/h" +
                                                        "\nRumbo: " + selectVehicle.Direction.ToString() + " °N" +
                                                        "\nReferencia:" + selectVehicle.GeoRef,
                                                        SimplexityVehicleMap.Imagenes.CamionNaranja3);
                  }
                if (selectVehicle.Deltac >= 60 &&
                    selectVehicle.Deltac < 120)
                  {
                    simplexityVehicleMap1.M02AddVehicle(selectVehicle.VehCode, selectVehicle.Latitude, selectVehicle.Longitude,
                                                        Color.FromArgb(248, 248, 23, 23), "Atraso 60 a 120 min",
                                                        "Registro :\n" +
                                                        selectVehicle.GpsDate +
                                                        "\nVel: " + selectVehicle.Speed.ToString() + " Km/h" +
                                                        "\nRumbo: " + selectVehicle.Direction.ToString() + " °N" +
                                                        "\nReferencia:" + selectVehicle.GeoRef,
                                                        SimplexityVehicleMap.Imagenes.CamionNaranja3);
                  }
                if (selectVehicle.Deltac >= 120)
                  {
                    simplexityVehicleMap1.M02AddVehicle(selectVehicle.VehCode, selectVehicle.Latitude, selectVehicle.Longitude,
                                                        Color.FromArgb(248, 65, 6, 47), "Atraso mas 120 min",
                                                        "Registro :\n" +
                                                        selectVehicle.GpsDate +
                                                        "\nVel: " + selectVehicle.Speed.ToString() + " Km/h" +
                                                        "\nRumbo: " + selectVehicle.Direction.ToString() + " °N" +
                                                        "\nReferencia:" + selectVehicle.GeoRef,
                                                        SimplexityVehicleMap.Imagenes.CamionNaranja3);
                  }
                if (selectVehicle.Deltac < 0)
                  {
                    simplexityVehicleMap1.M02AddVehicle(selectVehicle.VehCode, selectVehicle.Latitude, selectVehicle.Longitude,
                                                        Color.FromArgb(250, 90, 90, 90), "Sin Atraso",
                                                        "Registro :\n" +
                                                        selectVehicle.GpsDate +
                                                        "\nVel: " + selectVehicle.Speed.ToString() + " Km/h" +
                                                        "\nRumbo: " + selectVehicle.Direction.ToString() + " °N" +
                                                        "\nReferencia:" + selectVehicle.GeoRef,
                                                        SimplexityVehicleMap.Imagenes.CamionNaranja3);
                  }
              }

            //Thread.Sleep(5000);
            //var mt = new Thread(new ThreadStart(ConsumeGpsInfo));
            //mt.Start();
          }

        private void WscRequestVehicleGpsInfoCompleted(object sender, RequestVehicleGpsInfoCompletedEventArgs e)
          {
            var t = e.Result.ToList();
            FillData(t, true);
          }

        // Obsoleto  
        //private void ConsumeGpsInfo()
        //  {
        //    ////actualiza toda la info de gps
        //    //var wsc = new TraficoClient();
        //    ////WsNodeAccess.WsNodeAccessSoapClient wsc = new WsNodeAccessSoapClient();
        //    //wsc.RequestVehicleGpsInfoAsync(Sucursal, Empresa);
        //    //wsc.RequestVehicleGpsInfoCompleted +=
        //    //  new EventHandler<RequestVehicleGpsInfoCompletedEventArgs>(WscRequestVehicleGpsInfoCompletedB);
        //  }

        //private void WscRequestVehicleGpsInfoCompletedB(object sender, RequestVehicleGpsInfoCompletedEventArgs e)
        //{
        //  var t = e.Result.ToList();
        //  List<VehiclesGpsData> j = t;

        //  DispatcherOperation dispatcherOperation1 =
        //    label1a.Dispatcher.BeginInvoke(delegate() { label1a.Content = "Actualización\n" + DateTime.Now.ToString(); });
        //  foreach (var x in j)
        //  {
        //    var l = x.VehCode;
        //    Location g = new Location(Convert.ToDouble(x.Latitude), Convert.ToDouble(x.Longitude));

        //    //hay que buscar en la lista de d veh
        //    foreach (var y in simplexityVehicleMap1.ListaVehiculos.Where(y => y.Placa == x.VehCode))
        //    {
        //      DispatcherOperation dispatcherOperation =
        //        simplexityVehicleMap1.Dispatcher.BeginInvoke(delegate()
        //                                                       {
        //                                                         var br = (Border) y.Information.Children[2];
        //                                                         var ip = (TextBox) br.Child;
        //                                                         ip.Text = "Registro :\n" +
        //                                                                   x.GpsDate +
        //                                                                   "\nVel: " + x.Speed.ToString() + " Km/h" +
        //                                                                   "\nRumbo: " + x.Direction.ToString() + " °N" +
        //                                                                   "\nReferencia:" + x.GeoRef;
        //                                                         //((TextBox)y.Information.Children[2]).Text = "Hola";  
        //                                                         y.UbicaVeh(g.Latitude, g.Longitude);
        //                                                         if (y.MantenerCentrado == true)
        //                                                         {
        //                                                           simplexityVehicleMap1.M06CentrarVehiculo(y.Placa);
        //                                                         }
        //                                                       });
        //    }
        //    Thread.Sleep(300);
        //  }
        //  Thread.Sleep(30000);
        //  Thread mt = new Thread(new ThreadStart(ConsumeGpsInfo));
        //  mt.Start();
        //}

        private void LstVehiculosSelectionChanged(object sender, SelectionChangedEventArgs e)
          {
            if (lstVehiculos.SelectedIndex >= 0)
              {
                vehiclesGpsSelected = (VehiclesGpsData) lstVehiculos.SelectedItem;
                simplexityVehicleMap1.M06CentrarVehiculo(vehiclesGpsSelected.VehCode);
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == vehiclesGpsSelected.VehCode
                         select w).First();

                //es solo un resultado
                checkBox2.IsChecked = q.MantenerCentrado;
                checkBox4.IsChecked = q.Visible;
                checkBox3.IsChecked = q.AutoZoom;

                simplexityVehicleMap1.M03RemoveVehicle(vehiclesGpsSelected.VehCode);
                var myList = new List<VehiclesGpsData>();

                myList.Add(vehiclesGpsSelected);
                FillData(myList, false);
              }
          }

        private void txtVehiculo_TextChanged(object sender, TextChangedEventArgs e)
          {
            simplexityVehicleMap1.M06CentrarVehiculo(txtVehiculo.Text.ToUpper());
          }

        private void button1_Click(object sender, RoutedEventArgs e)
          {
            simplexityVehicleMap1.M08OcultarFlota(false);
          }

        private void button2_Click(object sender, RoutedEventArgs e)
          {
            simplexityVehicleMap1.M08OcultarFlota(true);
          }

        private void checkBox2_Checked_1(object sender, RoutedEventArgs e)
          {
            //mantener centrado
            if (lstVehiculos.SelectedIndex >= 0)
              {
                var tmp = (VehiclesGpsData) lstVehiculos.SelectedItem;
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                q.MantenerCentrado = true;
              }
          }

        private void checkBox2_Unchecked(object sender, RoutedEventArgs e)
          {
            //mantener centrado
            if (lstVehiculos.SelectedIndex >= 0)
              {
                var tmp = (VehiclesGpsData) lstVehiculos.SelectedItem;
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                q.MantenerCentrado = false;
              }
          }

        private void checkBox4_Checked(object sender, RoutedEventArgs e)
          {
            //mantener visible
            if (lstVehiculos.SelectedIndex >= 0)
              {
                var tmp = (VehiclesGpsData) lstVehiculos.SelectedItem;
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                q.Visible = true;
              }
          }

        private void checkBox4_Unchecked(object sender, RoutedEventArgs e)
          {
            //mantener visible
            if (lstVehiculos.SelectedIndex >= 0)
              {
                var tmp = (VehiclesGpsData) lstVehiculos.SelectedItem;
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                q.Visible = false;
              }
          }

        private void checkBox3_Checked(object sender, RoutedEventArgs e)
          {
            //mantener visible
            if (lstVehiculos.SelectedIndex >= 0)
              {
                var tmp = (VehiclesGpsData) lstVehiculos.SelectedItem;
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                q.AutoZoom = true;
              }
          }

        private void checkBox3_Unchecked(object sender, RoutedEventArgs e)
          {
            //mantener visible
            if (lstVehiculos.SelectedIndex >= 0)
              {
                var tmp = (VehiclesGpsData) lstVehiculos.SelectedItem;
                //en tmp.vehcode se tiene la placa seleccionada
                var q = (from w in simplexityVehicleMap1.ListaVehiculos
                         where w.Placa == tmp.VehCode
                         select w).First();

                //es solo un resultado
                q.AutoZoom = false;
              }
          }

        private void RadPaneGroup_SelectionChanged(object sender, Telerik.Windows.Controls.RadSelectionChangedEventArgs e)
          {
          }

      }
  }