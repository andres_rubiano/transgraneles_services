﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.18449
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This code was auto-generated by Microsoft.Silverlight.ServiceReference, version 5.0.61118.0
// 
namespace UI.ControlTrafico.Mapa.TraficoService {
    using System.Runtime.Serialization;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="VehiclesGpsData", Namespace="http://schemas.datacontract.org/2004/07/Simplexity.Solla.BusinessRules.Main.Trafi" +
        "co")]
    public partial class VehiclesGpsData : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int DeltacField;
        
        private double DirectionField;
        
        private string GeoRefField;
        
        private string GpsDateField;
        
        private double LatitudeField;
        
        private double LongitudeField;
        
        private double SpeedField;
        
        private string VehCodeField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Deltac {
            get {
                return this.DeltacField;
            }
            set {
                if ((this.DeltacField.Equals(value) != true)) {
                    this.DeltacField = value;
                    this.RaisePropertyChanged("Deltac");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Direction {
            get {
                return this.DirectionField;
            }
            set {
                if ((this.DirectionField.Equals(value) != true)) {
                    this.DirectionField = value;
                    this.RaisePropertyChanged("Direction");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string GeoRef {
            get {
                return this.GeoRefField;
            }
            set {
                if ((object.ReferenceEquals(this.GeoRefField, value) != true)) {
                    this.GeoRefField = value;
                    this.RaisePropertyChanged("GeoRef");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string GpsDate {
            get {
                return this.GpsDateField;
            }
            set {
                if ((object.ReferenceEquals(this.GpsDateField, value) != true)) {
                    this.GpsDateField = value;
                    this.RaisePropertyChanged("GpsDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Latitude {
            get {
                return this.LatitudeField;
            }
            set {
                if ((this.LatitudeField.Equals(value) != true)) {
                    this.LatitudeField = value;
                    this.RaisePropertyChanged("Latitude");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Longitude {
            get {
                return this.LongitudeField;
            }
            set {
                if ((this.LongitudeField.Equals(value) != true)) {
                    this.LongitudeField = value;
                    this.RaisePropertyChanged("Longitude");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Speed {
            get {
                return this.SpeedField;
            }
            set {
                if ((this.SpeedField.Equals(value) != true)) {
                    this.SpeedField = value;
                    this.RaisePropertyChanged("Speed");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string VehCode {
            get {
                return this.VehCodeField;
            }
            set {
                if ((object.ReferenceEquals(this.VehCodeField, value) != true)) {
                    this.VehCodeField = value;
                    this.RaisePropertyChanged("VehCode");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="TraficoService.ITrafico")]
    public interface ITrafico {
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/ITrafico/RequestVehicleGpsInfo", ReplyAction="http://tempuri.org/ITrafico/RequestVehicleGpsInfoResponse")]
        System.IAsyncResult BeginRequestVehicleGpsInfo(string sucursal, string empresa, string cliente, System.AsyncCallback callback, object asyncState);
        
        System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData> EndRequestVehicleGpsInfo(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/ITrafico/RequestVehicleGpsInfoWithoutTier", ReplyAction="http://tempuri.org/ITrafico/RequestVehicleGpsInfoWithoutTierResponse")]
        System.IAsyncResult BeginRequestVehicleGpsInfoWithoutTier(string sucursal, string empresa, System.AsyncCallback callback, object asyncState);
        
        System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData> EndRequestVehicleGpsInfoWithoutTier(System.IAsyncResult result);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ITraficoChannel : UI.ControlTrafico.Mapa.TraficoService.ITrafico, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class RequestVehicleGpsInfoCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        public RequestVehicleGpsInfoCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        public System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData> Result {
            get {
                base.RaiseExceptionIfNecessary();
                return ((System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData>)(this.results[0]));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class RequestVehicleGpsInfoWithoutTierCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        public RequestVehicleGpsInfoWithoutTierCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        public System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData> Result {
            get {
                base.RaiseExceptionIfNecessary();
                return ((System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData>)(this.results[0]));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class TraficoClient : System.ServiceModel.ClientBase<UI.ControlTrafico.Mapa.TraficoService.ITrafico>, UI.ControlTrafico.Mapa.TraficoService.ITrafico {
        
        private BeginOperationDelegate onBeginRequestVehicleGpsInfoDelegate;
        
        private EndOperationDelegate onEndRequestVehicleGpsInfoDelegate;
        
        private System.Threading.SendOrPostCallback onRequestVehicleGpsInfoCompletedDelegate;
        
        private BeginOperationDelegate onBeginRequestVehicleGpsInfoWithoutTierDelegate;
        
        private EndOperationDelegate onEndRequestVehicleGpsInfoWithoutTierDelegate;
        
        private System.Threading.SendOrPostCallback onRequestVehicleGpsInfoWithoutTierCompletedDelegate;
        
        private BeginOperationDelegate onBeginOpenDelegate;
        
        private EndOperationDelegate onEndOpenDelegate;
        
        private System.Threading.SendOrPostCallback onOpenCompletedDelegate;
        
        private BeginOperationDelegate onBeginCloseDelegate;
        
        private EndOperationDelegate onEndCloseDelegate;
        
        private System.Threading.SendOrPostCallback onCloseCompletedDelegate;
        
        public TraficoClient() {
        }
        
        public TraficoClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public TraficoClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TraficoClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public TraficoClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Net.CookieContainer CookieContainer {
            get {
                System.ServiceModel.Channels.IHttpCookieContainerManager httpCookieContainerManager = this.InnerChannel.GetProperty<System.ServiceModel.Channels.IHttpCookieContainerManager>();
                if ((httpCookieContainerManager != null)) {
                    return httpCookieContainerManager.CookieContainer;
                }
                else {
                    return null;
                }
            }
            set {
                System.ServiceModel.Channels.IHttpCookieContainerManager httpCookieContainerManager = this.InnerChannel.GetProperty<System.ServiceModel.Channels.IHttpCookieContainerManager>();
                if ((httpCookieContainerManager != null)) {
                    httpCookieContainerManager.CookieContainer = value;
                }
                else {
                    throw new System.InvalidOperationException("No se puede establecer el objeto CookieContainer. Asegúrese de que el enlace cont" +
                            "iene un objeto HttpCookieContainerBindingElement.");
                }
            }
        }
        
        public event System.EventHandler<RequestVehicleGpsInfoCompletedEventArgs> RequestVehicleGpsInfoCompleted;
        
        public event System.EventHandler<RequestVehicleGpsInfoWithoutTierCompletedEventArgs> RequestVehicleGpsInfoWithoutTierCompleted;
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> OpenCompleted;
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> CloseCompleted;
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.IAsyncResult UI.ControlTrafico.Mapa.TraficoService.ITrafico.BeginRequestVehicleGpsInfo(string sucursal, string empresa, string cliente, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginRequestVehicleGpsInfo(sucursal, empresa, cliente, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData> UI.ControlTrafico.Mapa.TraficoService.ITrafico.EndRequestVehicleGpsInfo(System.IAsyncResult result) {
            return base.Channel.EndRequestVehicleGpsInfo(result);
        }
        
        private System.IAsyncResult OnBeginRequestVehicleGpsInfo(object[] inValues, System.AsyncCallback callback, object asyncState) {
            string sucursal = ((string)(inValues[0]));
            string empresa = ((string)(inValues[1]));
            string cliente = ((string)(inValues[2]));
            return ((UI.ControlTrafico.Mapa.TraficoService.ITrafico)(this)).BeginRequestVehicleGpsInfo(sucursal, empresa, cliente, callback, asyncState);
        }
        
        private object[] OnEndRequestVehicleGpsInfo(System.IAsyncResult result) {
            System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData> retVal = ((UI.ControlTrafico.Mapa.TraficoService.ITrafico)(this)).EndRequestVehicleGpsInfo(result);
            return new object[] {
                    retVal};
        }
        
        private void OnRequestVehicleGpsInfoCompleted(object state) {
            if ((this.RequestVehicleGpsInfoCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.RequestVehicleGpsInfoCompleted(this, new RequestVehicleGpsInfoCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void RequestVehicleGpsInfoAsync(string sucursal, string empresa, string cliente) {
            this.RequestVehicleGpsInfoAsync(sucursal, empresa, cliente, null);
        }
        
        public void RequestVehicleGpsInfoAsync(string sucursal, string empresa, string cliente, object userState) {
            if ((this.onBeginRequestVehicleGpsInfoDelegate == null)) {
                this.onBeginRequestVehicleGpsInfoDelegate = new BeginOperationDelegate(this.OnBeginRequestVehicleGpsInfo);
            }
            if ((this.onEndRequestVehicleGpsInfoDelegate == null)) {
                this.onEndRequestVehicleGpsInfoDelegate = new EndOperationDelegate(this.OnEndRequestVehicleGpsInfo);
            }
            if ((this.onRequestVehicleGpsInfoCompletedDelegate == null)) {
                this.onRequestVehicleGpsInfoCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnRequestVehicleGpsInfoCompleted);
            }
            base.InvokeAsync(this.onBeginRequestVehicleGpsInfoDelegate, new object[] {
                        sucursal,
                        empresa,
                        cliente}, this.onEndRequestVehicleGpsInfoDelegate, this.onRequestVehicleGpsInfoCompletedDelegate, userState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.IAsyncResult UI.ControlTrafico.Mapa.TraficoService.ITrafico.BeginRequestVehicleGpsInfoWithoutTier(string sucursal, string empresa, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginRequestVehicleGpsInfoWithoutTier(sucursal, empresa, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData> UI.ControlTrafico.Mapa.TraficoService.ITrafico.EndRequestVehicleGpsInfoWithoutTier(System.IAsyncResult result) {
            return base.Channel.EndRequestVehicleGpsInfoWithoutTier(result);
        }
        
        private System.IAsyncResult OnBeginRequestVehicleGpsInfoWithoutTier(object[] inValues, System.AsyncCallback callback, object asyncState) {
            string sucursal = ((string)(inValues[0]));
            string empresa = ((string)(inValues[1]));
            return ((UI.ControlTrafico.Mapa.TraficoService.ITrafico)(this)).BeginRequestVehicleGpsInfoWithoutTier(sucursal, empresa, callback, asyncState);
        }
        
        private object[] OnEndRequestVehicleGpsInfoWithoutTier(System.IAsyncResult result) {
            System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData> retVal = ((UI.ControlTrafico.Mapa.TraficoService.ITrafico)(this)).EndRequestVehicleGpsInfoWithoutTier(result);
            return new object[] {
                    retVal};
        }
        
        private void OnRequestVehicleGpsInfoWithoutTierCompleted(object state) {
            if ((this.RequestVehicleGpsInfoWithoutTierCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.RequestVehicleGpsInfoWithoutTierCompleted(this, new RequestVehicleGpsInfoWithoutTierCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void RequestVehicleGpsInfoWithoutTierAsync(string sucursal, string empresa) {
            this.RequestVehicleGpsInfoWithoutTierAsync(sucursal, empresa, null);
        }
        
        public void RequestVehicleGpsInfoWithoutTierAsync(string sucursal, string empresa, object userState) {
            if ((this.onBeginRequestVehicleGpsInfoWithoutTierDelegate == null)) {
                this.onBeginRequestVehicleGpsInfoWithoutTierDelegate = new BeginOperationDelegate(this.OnBeginRequestVehicleGpsInfoWithoutTier);
            }
            if ((this.onEndRequestVehicleGpsInfoWithoutTierDelegate == null)) {
                this.onEndRequestVehicleGpsInfoWithoutTierDelegate = new EndOperationDelegate(this.OnEndRequestVehicleGpsInfoWithoutTier);
            }
            if ((this.onRequestVehicleGpsInfoWithoutTierCompletedDelegate == null)) {
                this.onRequestVehicleGpsInfoWithoutTierCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnRequestVehicleGpsInfoWithoutTierCompleted);
            }
            base.InvokeAsync(this.onBeginRequestVehicleGpsInfoWithoutTierDelegate, new object[] {
                        sucursal,
                        empresa}, this.onEndRequestVehicleGpsInfoWithoutTierDelegate, this.onRequestVehicleGpsInfoWithoutTierCompletedDelegate, userState);
        }
        
        private System.IAsyncResult OnBeginOpen(object[] inValues, System.AsyncCallback callback, object asyncState) {
            return ((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(callback, asyncState);
        }
        
        private object[] OnEndOpen(System.IAsyncResult result) {
            ((System.ServiceModel.ICommunicationObject)(this)).EndOpen(result);
            return null;
        }
        
        private void OnOpenCompleted(object state) {
            if ((this.OpenCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.OpenCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void OpenAsync() {
            this.OpenAsync(null);
        }
        
        public void OpenAsync(object userState) {
            if ((this.onBeginOpenDelegate == null)) {
                this.onBeginOpenDelegate = new BeginOperationDelegate(this.OnBeginOpen);
            }
            if ((this.onEndOpenDelegate == null)) {
                this.onEndOpenDelegate = new EndOperationDelegate(this.OnEndOpen);
            }
            if ((this.onOpenCompletedDelegate == null)) {
                this.onOpenCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnOpenCompleted);
            }
            base.InvokeAsync(this.onBeginOpenDelegate, null, this.onEndOpenDelegate, this.onOpenCompletedDelegate, userState);
        }
        
        private System.IAsyncResult OnBeginClose(object[] inValues, System.AsyncCallback callback, object asyncState) {
            return ((System.ServiceModel.ICommunicationObject)(this)).BeginClose(callback, asyncState);
        }
        
        private object[] OnEndClose(System.IAsyncResult result) {
            ((System.ServiceModel.ICommunicationObject)(this)).EndClose(result);
            return null;
        }
        
        private void OnCloseCompleted(object state) {
            if ((this.CloseCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.CloseCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void CloseAsync() {
            this.CloseAsync(null);
        }
        
        public void CloseAsync(object userState) {
            if ((this.onBeginCloseDelegate == null)) {
                this.onBeginCloseDelegate = new BeginOperationDelegate(this.OnBeginClose);
            }
            if ((this.onEndCloseDelegate == null)) {
                this.onEndCloseDelegate = new EndOperationDelegate(this.OnEndClose);
            }
            if ((this.onCloseCompletedDelegate == null)) {
                this.onCloseCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnCloseCompleted);
            }
            base.InvokeAsync(this.onBeginCloseDelegate, null, this.onEndCloseDelegate, this.onCloseCompletedDelegate, userState);
        }
        
        protected override UI.ControlTrafico.Mapa.TraficoService.ITrafico CreateChannel() {
            return new TraficoClientChannel(this);
        }
        
        private class TraficoClientChannel : ChannelBase<UI.ControlTrafico.Mapa.TraficoService.ITrafico>, UI.ControlTrafico.Mapa.TraficoService.ITrafico {
            
            public TraficoClientChannel(System.ServiceModel.ClientBase<UI.ControlTrafico.Mapa.TraficoService.ITrafico> client) : 
                    base(client) {
            }
            
            public System.IAsyncResult BeginRequestVehicleGpsInfo(string sucursal, string empresa, string cliente, System.AsyncCallback callback, object asyncState) {
                object[] _args = new object[3];
                _args[0] = sucursal;
                _args[1] = empresa;
                _args[2] = cliente;
                System.IAsyncResult _result = base.BeginInvoke("RequestVehicleGpsInfo", _args, callback, asyncState);
                return _result;
            }
            
            public System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData> EndRequestVehicleGpsInfo(System.IAsyncResult result) {
                object[] _args = new object[0];
                System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData> _result = ((System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData>)(base.EndInvoke("RequestVehicleGpsInfo", _args, result)));
                return _result;
            }
            
            public System.IAsyncResult BeginRequestVehicleGpsInfoWithoutTier(string sucursal, string empresa, System.AsyncCallback callback, object asyncState) {
                object[] _args = new object[2];
                _args[0] = sucursal;
                _args[1] = empresa;
                System.IAsyncResult _result = base.BeginInvoke("RequestVehicleGpsInfoWithoutTier", _args, callback, asyncState);
                return _result;
            }
            
            public System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData> EndRequestVehicleGpsInfoWithoutTier(System.IAsyncResult result) {
                object[] _args = new object[0];
                System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData> _result = ((System.Collections.ObjectModel.ObservableCollection<UI.ControlTrafico.Mapa.TraficoService.VehiclesGpsData>)(base.EndInvoke("RequestVehicleGpsInfoWithoutTier", _args, result)));
                return _result;
            }
        }
    }
}
