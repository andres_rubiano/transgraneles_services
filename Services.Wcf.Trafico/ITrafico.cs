﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Simplexity.Solla.BusinessRules.Main.Trafico;

namespace Services.Wcf.Trafico
{
  [ServiceContract]
  public interface ITrafico
  {
    [OperationContract]
    List<VehiclesGpsData> RequestVehicleGpsInfo(string sucursal, string empresa, string cliente);
    
    [OperationContract]
    List<VehiclesGpsData> RequestVehicleGpsInfoWithoutTier(string sucursal, string empresa);
  }
}