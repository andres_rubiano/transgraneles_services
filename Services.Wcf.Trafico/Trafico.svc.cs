﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Simplexity.Solla.BusinessRules.Main.Trafico;
using Infrastructure.Data.Main.EF;
using Simplexity.Solla.Common;

namespace Services.Wcf.Trafico
  {
    public class Trafico : ITrafico
      {
        public List<VehiclesGpsData> RequestVehicleGpsInfo(string sucursal, string empresa, string cliente)
          {
            var listV = new List<VehiclesGpsData>();
            try
              {
                using (var db = new SollaTraficoEntities())
                  {
                    var trafico = from n in db.vTRAFICO_01
                                  where n.TIECODE.Equals(cliente)
                                  select n;

                    foreach (var vTrafico01 in trafico)
                      {
                        if (vTrafico01.FFPLAT.ToString().Equals("") ||
                            vTrafico01.FFPLONG.ToString().Equals(""))
                          {
                            continue;
                          }
                        if (Convert.ToDouble(vTrafico01.FFPLAT.ToString()) != 0 &&
                            Convert.ToDouble(vTrafico01.FFPLONG.ToString()) != 0)
                          {
                            var fecha = vTrafico01.OTMDATEP.ToString();

                            var lat = Utility.StringToDouble(vTrafico01.FFPLAT.ToString());
                            var lon = Utility.StringToDouble(vTrafico01.FFPLONG.ToString());

                            //var lat = Convert.ToDouble(vTrafico01.FFPLAT.Replace(",", "."));
                            //var lon = Convert.ToDouble(vTrafico01.FFPLONG.Replace(",", "."));

                          var deltac = Convert.ToInt32(vTrafico01.DELTAC);
                            var texto = "Viaje: " + vTrafico01.VOYNUM.ToString() + "\n" +
                                        "Remesa: " + vTrafico01.OTPNUM.ToString() + "\n" +
                                        "Conductor: " + vTrafico01.VOYCHSALNOM.ToString() + "\n" +
                                        "Celular: " + vTrafico01.VOYSALTELMOB.ToString() + "\n" +
                                        "Origen: " + vTrafico01.FFRDEPVILLIB.ToString() + "\n" +
                                        "Destino: " + vTrafico01.FFRARRVILLIB.ToString() + "\n" +
                                        "Puesto de control:" + vTrafico01.OTMLIB.ToString() + "\n" +
                                        "Fecha estimada: " + vTrafico01.OTMDATEE.ToString() + "\n";
                            var placa = vTrafico01.VOYTRACODE.ToString();

                            var h = new VehiclesGpsData
                                      {
                                        VehCode = placa,
                                        Latitude = lat,
                                        Longitude = lon,
                                        Direction = 0,
                                        GpsDate = fecha,
                                        GeoRef = texto,
                                        Speed = 0,
                                        Deltac = deltac
                                      };
                            var target = listV.Any(a => a.VehCode == h.VehCode);
                            if (!target)
                              {
                                listV.Add(h);
                              }
                          }
                      }
                  }
              }
            catch (Exception ex)
              {
                throw ex;
              }
            return listV;
          }

        public List<VehiclesGpsData> RequestVehicleGpsInfoWithoutTier(string sucursal, string empresa)
          {
            string sQuery;
            DataTable myDataTable;
            var listV = new List<VehiclesGpsData>();
            try
              {
                using (var db = new SollaTraficoEntities())
                  {
                    var trafico = db.vTRAFICO_00;
                    foreach (var vTrafico00 in trafico)
                      {
                        if (vTrafico00.FFPLAT.ToString().Equals("") ||
                            vTrafico00.FFPLONG.ToString().Equals(""))
                          {
                            continue;
                          }
                        if (Convert.ToDouble(vTrafico00.FFPLAT.ToString()) != 0 &&
                            Convert.ToDouble(vTrafico00.FFPLONG.ToString()) != 0)
                          {
                            var fecha = vTrafico00.OTMDATEP.ToString();

                            //var strlat = vTrafico00.FFPLAT.ToString().Replace(".", ",");
                            ////var strlat = vTrafico00.FFPLAT.ToString();
                            //var lat = Convert.ToDouble(strlat);
                            //var strlon = vTrafico00.FFPLONG.ToString().Replace(".", ",");
                            ////var strlon = vTrafico00.FFPLONG.ToString();
                            //var lon = Convert.ToDouble(strlon);

                            var lat = Utility.StringToDouble(vTrafico00.FFPLAT.ToString());
                            var lon = Utility.StringToDouble(vTrafico00.FFPLONG.ToString());

                            var deltac = Convert.ToInt32(vTrafico00.DELTAC);
                            var texto = "Viaje: " + vTrafico00.VOYNUM.ToString() + "\n" +
                                        "Conductor: " + vTrafico00.VOYCHSALNOM.ToString() + "\n" +
                                        "Celular: " + vTrafico00.VOYSALTELMOB.ToString() + "\n" +
                                        "Origen: " + vTrafico00.FFRDEPVILLIB.ToString() + "\n" +
                                        "Destino: " + vTrafico00.FFRARRVILLIB.ToString() + "\n" +
                                        "Puesto de control:" + vTrafico00.OTMLIB.ToString() + "\n" +
                                        "Fecha estimada: " + vTrafico00.OTMDATEE.ToString() + "\n";
                            var placa = vTrafico00.VOYTRACODE.ToString();

                            var h = new VehiclesGpsData
                                      {
                                        VehCode = placa,
                                        Latitude = lat,
                                        Longitude = lon,
                                        Direction = 0,
                                        GpsDate = fecha,
                                        GeoRef = texto,
                                        Speed = 0,
                                        Deltac = deltac
                                      };
                            listV.Add(h);
                          }
                      }
                  }
              }
            catch (Exception ex)
              {
                throw ex;
              }
            return listV;
          }
      }
  }