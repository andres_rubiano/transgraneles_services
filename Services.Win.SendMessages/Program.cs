﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mime;
using System.ServiceProcess;
using System.Text;
using Topshelf;

namespace Services.Win.SendMessages
{
    static class Program
    {
        static void Main(string[] args)
        {
            string Description = ConfigurationManager.AppSettings["ServiceDescription"].ToString();
            string DisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"].ToString();
            string ServiceName = ConfigurationManager.AppSettings["ServiceName"].ToString();

            HostFactory.Run(x =>
            {
                x.Service<ServiceFe>(s =>
                {
                    s.ConstructUsing(name => new ServiceFe());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.StartAutomaticallyDelayed();
                x.RunAsLocalSystem();
                x.SetDescription(string.Format("{0} {1}", Description, ""));
                x.SetDisplayName(string.Format("{0} {1}", DisplayName, ""));
                x.SetServiceName(ServiceName);
            });

            Console.ReadLine();
        }
    }
}
