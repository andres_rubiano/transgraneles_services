﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Configuration;
using System.Timers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Simplexity.Solla.BusinessRules.Main.Services;


namespace Services.Win.SendMessages
{
  partial class ServiceFe
  {
    private Timer _timer;
    private static MessagesService _messageService;
    private int _interval = 0;
      

    public ServiceFe()
    {
      //InitializeComponent();
    }

    public void Start()
    {
      StartService();
    }

    private static void ProcessRequests()
    {
      try
      {
        _messageService.ProcessPendingMessages();
      }
      catch (Exception ex)
      {

        ExceptionPolicy.HandleException(ex, "LoggingReplaceException");

      }
    }

    public void StartService()
    {
        _messageService = new MessagesService();

      try
      {
          int.TryParse(ConfigurationManager.AppSettings["TimerInterval"], out _interval);

          _timer = new Timer();
          _timer.Elapsed += TimerElapsed;
          _timer.Interval = _interval;
          _timer.Enabled = true;
        
      }
      catch (Exception ex)
      {

        ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
      }
    }

    public void Stop()
    {
      Logger.Write(Resources.ReasonStopService, "General", 0, 777, TraceEventType.Information, "FaxSenderInfo");      
      _messageService.Dispose();
    }

    private void TimerElapsed(object sender, ElapsedEventArgs e)
    {
      _timer.Enabled = false;
      ProcessRequests();
      _timer.Enabled = true;
    }
  }
}
