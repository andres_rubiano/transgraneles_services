﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Map;
using Path = System.Windows.Shapes.Path;

namespace UI.ControlTrafico.UserControls
{
    public partial class SimplexityVehicleMap : UserControl
    {
        //definiciones varias
        //lista para guardar los vehiculos ingresados
        public List<Vehiculo> ListaVehiculos = new List<Vehiculo>();
        //lista donde se guarda las rutas 
        public List<LineaRuta> ListaRutas = new List<LineaRuta>();
        //lista donde se almacena
        public List<MapPinPoint> ListaPuntosRuta = new List<MapPinPoint>();
        private List<Vehiculo> _rpathOrg;
        private Color _colorPathOrg;

        //seleccion de iconos de presentacion
        public enum Imagenes
        {
            AmbulanciaRoja,
            VolquetaCarbon,
            RetroEscavadora,
            Bomberos,
            Montacarga,
            CamionVerde,
            Mezcladora,
            Carrotanque1,
            CamionNaranja,
            GruaNaranja,
            CamioncitoGris,
            CamionNaranja2,
            CamionNaranja3,
            CamionGris2,
            PuenteGrua
        }

        //lista que guarda los elemnetos cargados desde KML
        public List<FrameworkElement> KmlFileElemnts = new List<FrameworkElement>();

        private string ObtImg(Imagenes c)
        {
            string rutaImg = "";

            switch (c)
            {
                case Imagenes.AmbulanciaRoja:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/Ambulance_Red.png";
                    break;
                case Imagenes.VolquetaCarbon:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/dumper-icon.png";
                    break;
                case Imagenes.RetroEscavadora:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/Excavator_Yellow.png";
                    break;
                case Imagenes.Bomberos:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/FireTruck-icon.png";
                    break;
                case Imagenes.Montacarga:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/ForkliftTruck-Loaded-icon.png";
                    break;
                case Imagenes.CamionVerde:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/Lorry-icon.png";
                    break;
                case Imagenes.Mezcladora:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/MixerTruck-icon.png";
                    break;
                case Imagenes.Carrotanque1:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/oil1.png";
                    break;
                case Imagenes.CamionNaranja:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/rss-icon.png";
                    break;
                case Imagenes.GruaNaranja:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/TowTruck_Yellow.png";
                    break;
                case Imagenes.CamioncitoGris:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/Truck-icon 2.png";
                    break;
                case Imagenes.CamionNaranja2:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/Truck-icon.png";
                    break;
                case Imagenes.CamionNaranja3:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/Truck-icon48.png";
                    break;
                case Imagenes.CamionGris2:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/truck-icon1.png";
                    break;
                case Imagenes.PuenteGrua:
                    rutaImg = "/UI.ControlTrafico.UserControls;component/Images/TruckMountedCrane__Yellow.png";
                    break;
            }
            return rutaImg;
        }

        public SimplexityVehicleMap()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Rutina de inicio del mapa
        /// Recuerde que este se debe llamar solo cuando la 
        /// applicacion inicie su ejecución
        /// </summary>
        public void M00StartAppMap()
        {
            M01StartMapProviderOsp();

        }

        /// <summary>
        /// Rutina para la inicialización del proveedor de mapas OpenstreetMaps
        /// </summary>
        private void M01StartMapProviderOsp()
        {
            radBusyIndicator.IsBusy = true;
            var pMapa = new OpenStreetMapProvider();
            //var mapP = new OpenStreetMapProvider();
            this.radMap1.Provider = pMapa;
            this.radMap1.Center = new Location(4.6802, -74.023);
            this.radMap1.ZoomLevel = 7;
            radBusyIndicator.IsBusy = false;
        }

        /// <summary>
        /// Agrega un vehiculo para visualizarlo en el mapa
        /// </summary>
        /// <param name="latitud">latitud en grados ej: 4.6 </param>
        /// <param name="longitud">longitud en grados ej: 74.2</param>
        /// <param name="placa">identificacion del vehiculo</param>
        /// <param name="colorEstado">color que identifica el estado del vehiculo</param>
        /// <param name="estado">texto que identifica el estado</param>
        /// <param name="infoText">texto de informacion como carga fechas y etc</param>
        public void M02AddVehicle(string placa, double latitud, double longitud, Color colorEstado, string estado, string infoText, Imagenes c)
          {

            var widthValue = 320;

            string imgVehicle = ObtImg(c);
            var cv = new Canvas {Width = 100, Height = 100};
          
            var ph = new Path();

            const string yAxisDataString = "M978.6509,491.334L958.6109,491.334L954.4549,500.874L949.9999,491.334L930.6259,491.334C928.4169,491.334,926.6259,489.543,926.6259,487.334L926.6259,433.272C926.6259,431.063,928.4169,429.272,930.6259,429.272L978.6509,429.272C980.8599,429.272,982.6509,431.063,982.6509,433.272L982.6509,487.334C982.6509,489.543,980.8599,491.334,978.6509,491.334z";

            const string nsPath = "<Path xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" Data=\"";
            ph = (Path)XamlReader.Load(nsPath + yAxisDataString + "\"/>");

            ph.Width = 67;
            ph.Height = 35;
            ph.StrokeStartLineCap = PenLineCap.Flat;
            ph.Stretch = Stretch.Fill;
            ph.StrokeEndLineCap = PenLineCap.Flat;
            ph.Stroke = new SolidColorBrush(Colors.White); //new Brush(Color.FromArgb(0x7F,00,00,00));
            ph.StrokeThickness = 1;
            ph.StrokeMiterLimit = 10;
            ph.StrokeLineJoin = PenLineJoin.Miter;
            ph.Fill = new SolidColorBrush(colorEstado);//new SolidColorBrush(Color.FromArgb(0x7F, 00, 00, 00));
            cv.Children.Add(ph);

            var tb = new TextBlock
                               {
                                   HorizontalAlignment = HorizontalAlignment.Center,
                                   VerticalAlignment = VerticalAlignment.Center,
                                   Margin = new Thickness(5),
                                   TextAlignment = TextAlignment.Center,
                                   Foreground = new SolidColorBrush(Colors.White),
                                   FontSize = 9,
                                   Width = 50,
                                   Height = 20,
                                   Text = placa
                               };
            cv.Children.Add(tb);

            var pinpoint = new MapPinPoint()
            {
                //Background = new SolidColorBrush(Colors.White),
                Foreground = new SolidColorBrush(Colors.Red),
                FontSize = 7,
                ImageSource = new BitmapImage(new Uri(imgVehicle, UriKind.RelativeOrAbsolute))
            };
            
            Border br = new Border();
            br.Width = 67;
            br.Height = 35;
            //br.Background = new SolidColorBrush(Colors.Purple);
            br.Child = cv;
            HotSpot hsp = new HotSpot();
            hsp.X = 0.5;
            hsp.Y = 1;
            MapLayer.SetHotSpot(br, hsp);

            HotSpot hspp = new HotSpot();
            hspp.X = 0.2;
            hspp.Y = 0.3;
            MapLayer.SetHotSpot(pinpoint, hspp);

            MapLayer.SetZoomRange(pinpoint, new ZoomRange(15, 22));
            //cv.Children.Add(pinpoint); 

            //colocar el tooltip del vehiculo

            Canvas cnT = new Canvas();
            cnT.Width = widthValue;
            cnT.Height = 120;

            Grid grtool = new Grid();
            Border brdt = new Border();

            // placa

            brdt.Width = widthValue;
            brdt.Height = 35;
            brdt.CornerRadius = new CornerRadius(5);
            brdt.Background = new SolidColorBrush(Color.FromArgb(150, 0x52, 0xA2, 0xD4));
            brdt.HorizontalAlignment = HorizontalAlignment.Center;

            TextBlock tblp = new TextBlock();
            tblp.Text = "Placa : " + placa;
            tblp.FontSize = 14;
            tblp.HorizontalAlignment = HorizontalAlignment.Center;
            tblp.VerticalAlignment = VerticalAlignment.Center;
            tblp.Foreground = new SolidColorBrush(Colors.White);
            tblp.FontWeight = FontWeights.Bold;
            brdt.Child = tblp;

            grtool.RowDefinitions.Add(new RowDefinition());
            grtool.Children.Add(brdt);
            Grid.SetRow(brdt, 0);
            
            // Estado

            Border brdt2 = new Border();
            brdt2.Background = new SolidColorBrush(Colors.White);
            brdt2.Width = widthValue;
            brdt2.Height = 25;
            brdt2.CornerRadius = new CornerRadius(5);
            brdt2.HorizontalAlignment = HorizontalAlignment.Center;

            tblp = new TextBlock();
            tblp.Text = "Estado : " + estado;
            tblp.FontSize = 11;
            tblp.HorizontalAlignment = HorizontalAlignment.Center;
            tblp.VerticalAlignment = VerticalAlignment.Center;
            tblp.TextAlignment = TextAlignment.Left;
            tblp.Foreground = new SolidColorBrush(Colors.Black);
            brdt2.Child = tblp;

            grtool.RowDefinitions.Add(new RowDefinition());
            grtool.Children.Add(brdt2);
            Grid.SetRow(brdt2, 1);

            // Información / detalle

            brdt2 = new Border();
            brdt2.Background = new SolidColorBrush(Color.FromArgb(230, 247, 247, 182));
            brdt2.Width = widthValue;
            brdt2.Height = 220;
            brdt2.CornerRadius = new CornerRadius(5);
            brdt2.HorizontalAlignment = HorizontalAlignment.Center;

            TextBox tbx = new TextBox();
            tbx.TextWrapping = TextWrapping.Wrap;
            tbx.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            tbx.Height = 200;
            tbx.Width = widthValue - 10;
            tbx.HorizontalAlignment = HorizontalAlignment.Center;
            tbx.VerticalAlignment = VerticalAlignment.Center;
            tbx.TextWrapping = TextWrapping.Wrap;
            tbx.Text = infoText;
            brdt2.Child = tbx;

            grtool.RowDefinitions.Add(new RowDefinition());
            grtool.Children.Add(brdt2);
            Grid.SetRow(brdt2, 2);
          
            ToolTipService.SetToolTip(br, grtool);
            ToolTipService.SetToolTip(pinpoint, grtool);

            //grtool es el tooltip

            //DoubleAnimation myDoubleAnimation = new DoubleAnimation();
            //myDoubleAnimation.From = 0.0;
            //myDoubleAnimation.To = 1.0;
            //myDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(5));
            //Storyboard myStoryboard;
            //myStoryboard = new Storyboard();
            //myStoryboard.Children.Add(myDoubleAnimation);
            //Storyboard.SetTargetName(myDoubleAnimation, grtool.Name);
            //grtool.Loaded +=new RoutedEventHandler(grtool_Loaded);

            MapLayer.SetLocation(br, new Location(latitud, longitud));

            MapLayer.SetLocation(pinpoint, new Location(latitud, longitud));
         
            Vehiculo tmpV = new Vehiculo();
            tmpV.Placa = placa;
            tmpV.PuntoGps = new Location(latitud, longitud);
            tmpV.Punto = pinpoint;

            tmpV.MantenerCentrado = false;
            tmpV.Tag = br;
            tmpV.Visible = true;
            tmpV.AutoZoom = false;
            tmpV.Information = grtool;

            infoLayer.Items.Add(br);
            infoLayer.Items.Add(pinpoint);

            ListaVehiculos.Add(tmpV);
        }

        /// <summary>
        /// remueve el vehiculo seleccionado
        /// </summary>
        /// <param name="placa">Placa que identifica el vehiculo a remover</param>
        public void M03RemoveVehicle(string placa)
        {
            int i = 0;
            foreach (Vehiculo y in ListaVehiculos)
            {
                if (y.Placa == placa)
                {
                    infoLayer.Items.Remove(y.Punto);
                    infoLayer.Items.Remove(y.Tag);
                    ListaVehiculos.Remove(y);
                    break;
                }
                i++;
            }
        }

        /// <summary>
        /// remueve todos los vehiculos
        /// </summary>
        public void M03RemoveVehicle()
        {
            
            foreach (Vehiculo y in ListaVehiculos)
            {
                
                    infoLayer.Items.Remove(y.Punto);
                    infoLayer.Items.Remove(y.Tag);
                    ListaVehiculos.Remove(y);
                    break;
                
            }
        }

        /// <summary>
        /// importa archivo kml con infomacion suministrada 
        /// en google earth
        /// </summary>
        /// <param name="pathFile">ruta al archivo KML</param>
        public void M04LoadKmlFile(System.IO.FileStream fs)
        {
            string description = "";
            //System.IO.FileStream fs = new FileStream(pathFile, FileMode.Open);
            System.IO.Stream fileStream = fs;
            List<FrameworkElement> elements = KmlReader.Read(fileStream);
            foreach (FrameworkElement element in elements)
            {
                //this.infoLayer.Items.Add(element);
                MapPinPoint point = element as MapPinPoint;
                if (point != null)
                {
                    // get description
                    // Placemark.Name could be used to get the Name of Placemark
                    description = (string)point.ExtendedData.GetValue("Placemark.Description");
                    if (description == null)
                    {
                        description = "";
                    }

                    // format description to a control
                    ContentControl control = this.GetFormattedDescription(description);

                    // register the FormattedDescription property
                    point.ExtendedData.PropertySet.RegisterProperty("FormattedDescription", "", typeof(ContentControl), null);

                    // add the control to ExtendedData
                    point.ExtendedData.SetValue("FormattedDescription", control);
                }
                HotSpot hspp = new HotSpot();
                hspp.X = 0.5;
                hspp.Y = 1;
                MapLayer.SetHotSpot(element, hspp);

                Border br = new Border();
                //br.Background = new SolidColorBrush(Color.FromArgb(0xA0, 0xC2, 0xD6, 0xE2));
                br.CornerRadius = new CornerRadius(4);
                TextBlock tx = new TextBlock();
                tx.Text = description;
                br.Child = tx;
                ToolTipService.SetToolTip(element, br);
                this.infoLayer.Items.Add(element);
                KmlFileElemnts.Add(element);
            }

            fileStream.Close();

        }

        /// <summary>
        /// Remueve KML de la capa de presentacion
        /// </summary>
        public void M05RemoveKml()
        {
            foreach (var e in KmlFileElemnts)
            {
                this.infoLayer.Items.Remove(e);

            }
            KmlFileElemnts.Clear();

        }

        /// <summary>
        /// Pinta una ruta a partir de una suscion de puntos
        /// </summary>
        /// <param name="placa">placa que identifica al vehiculo</param>
        /// <param name="rpath">sucesion de puntos que componen la ruta</param>
        /// <param name="colorPath">color que identifica la ruta</param>
        /// <param name="InfoGeneral">Informacion adicional para ser presentada en tootltip</param>
        public void M06DrawingRoute(string placa, List<Vehiculo> rpath, Color colorPath, string InfoGeneral)
        {

            radBusyIndicator.IsBusy = true;
            _rpathOrg = rpath;
            _colorPathOrg = colorPath;
            //var backgroundWorker = new BackgroundWorker();
            //backgroundWorker.DoWork += this.OnBackgroundWorkerDoWork;
            //backgroundWorker.RunWorkerCompleted += OnBackgroundWorkerRunWorkerCompleted;

            LineaRuta ln = new LineaRuta();
            ln.Stroke = new SolidColorBrush(colorPath);
            ln.StrokeThickness = 4;
            ln.Points = new LocationCollection();
            ln.Placa = placa;
            foreach (Vehiculo t in rpath)
            {
                ln.Points.Add(t.PuntoGps);
                M07AddPunto(t.Placa, t.PuntoGps.Latitude, t.PuntoGps.Longitude, Colors.Green, "sin estado",
                            t.DescrpcionContenido, "/Simplexity.Common.Controls;component/Images/r1b.png");
            }
            //colocar el tooltip

            Border br = new Border();
            br.CornerRadius = new CornerRadius(8);
            TextBlock tx = new TextBlock();
            tx.Text = "[" + placa + "] \n" + InfoGeneral;
            br.Child = tx;
            ToolTipService.SetToolTip(ln, br);
            ListaRutas.Add(ln);
            infoLayer.Items.Add(ln);
            radBusyIndicator.IsBusy = false;
        }

        /// <summary>
        /// Elimina ruta de la capa de presentacion
        /// </summary>
        /// <param name="placa">placa que identifica el vehiculo con su ruta</param>
        public void M07RemoveRoute(string placa)
        {
            foreach (var e in ListaRutas.Where(e => e.Placa == placa))
            {
                //
                this.infoLayer.Items.Remove(e);
                ListaRutas.Remove(e);
                break;
            }
            this.dynamicLayer.Items.Clear();
            ListaPuntosRuta.Clear();
            //foreach(var d in ListaPuntosRuta )
            //{
            //    this.dynamicLayer.Items.Remove(d);

            //}

        }

        /// <summary>
        /// Centra un vehiculo en la pantalla
        /// </summary>
        /// <param name="placa"></param>
        public void M06CentrarVehiculo(string placa)
        {
            var v = from q in ListaVehiculos where q.Placa == placa select q;
            foreach (Vehiculo z in v)
            {
              radMap1.Center = z.PuntoGps;

                if (z.AutoZoom == true)
                {
                    radMap1.ZoomLevel = 17;  
                }
                
                break;
            }
        }

        public void M08OcultarFlota(bool value)
        {
            foreach (Vehiculo y in ListaVehiculos)
            {
                y.Visible = value;                
            }
        }

        #region "codigos internos"

        //definiciones de codigo privado
        private void ocupado(bool vale)
        {
            radBusyIndicator.IsBusy = vale;
        }

        private void M07AddPunto(string placa, double latitud, double longitud, Color colorEstado, string estado, string infoText, string imgVehicle)
        {
            Canvas cv = new Canvas();
            cv.Width = 100;
            cv.Height = 100;

            Path ph = new Path();

            String yAxisDataString = "M978.6509,491.334L958.6109,491.334L954.4549,500.874L949.9999,491.334L930.6259,491.334C928.4169,491.334,926.6259,489.543,926.6259,487.334L926.6259,433.272C926.6259,431.063,928.4169,429.272,930.6259,429.272L978.6509,429.272C980.8599,429.272,982.6509,431.063,982.6509,433.272L982.6509,487.334C982.6509,489.543,980.8599,491.334,978.6509,491.334z";

            string nsPath = "<Path xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" Data=\"";
            ph = (Path)XamlReader.Load(nsPath + yAxisDataString + "\"/>");

            ph.Width = 67;
            ph.Height = 35;
            ph.StrokeStartLineCap = PenLineCap.Flat;
            ph.Stretch = Stretch.Fill;
            ph.StrokeEndLineCap = PenLineCap.Flat;
            ph.Stroke = new SolidColorBrush(Colors.White); //new Brush(Color.FromArgb(0x7F,00,00,00));
            ph.StrokeThickness = 1;
            ph.StrokeMiterLimit = 10;
            ph.StrokeLineJoin = PenLineJoin.Miter;
            ph.Fill = new SolidColorBrush(colorEstado);//new SolidColorBrush(Color.FromArgb(0x7F, 00, 00, 00));
            cv.Children.Add(ph);

            TextBlock tb = new TextBlock();
            tb.HorizontalAlignment = HorizontalAlignment.Center;
            tb.VerticalAlignment = VerticalAlignment.Center;
            tb.Margin = new Thickness(5);
            tb.TextAlignment = TextAlignment.Center;
            tb.Foreground = new SolidColorBrush(Colors.White);
            tb.FontSize = 12;
            tb.Width = 55;
            tb.Height = 25;
            tb.Text = placa;
            cv.Children.Add(tb);

            var pinpoint = new MapPinPoint()
            {
                //Background = new SolidColorBrush(Colors.White),
                Foreground = new SolidColorBrush(Colors.Red),
                FontSize = 7,
                ImageSource = new BitmapImage(new Uri(imgVehicle, UriKind.RelativeOrAbsolute)),
                ImageScale = 0.35,
            };

            Border br = new Border();
            br.Width = 67;
            br.Height = 35;
            //br.Background = new SolidColorBrush(Colors.Purple);
            br.Child = cv;
            HotSpot hsp = new HotSpot();
            hsp.X = 0.5;
            hsp.Y = 1;
            MapLayer.SetHotSpot(br, hsp);

            //HotSpot hspp = new HotSpot();
            //hspp.X = 0.2;
            //hspp.Y = 0.3;
            //MapLayer.SetHotSpot(pinpoint, hspp);

            MapLayer.SetZoomRange(pinpoint, new ZoomRange(17, 22));
            //cv.Children.Add(pinpoint); 

            //colocar el tooltip del vehiculo
            Canvas cnT = new Canvas();
            cnT.Width = 200;
            cnT.Height = 120;

            Grid grtool = new Grid();

            Border brdt = new Border();

            brdt.Width = 200;
            brdt.Height = 35;
            brdt.CornerRadius = new CornerRadius(5);
            brdt.Background = new SolidColorBrush(Color.FromArgb(150, 0x52, 0xA2, 0xD4));
            brdt.HorizontalAlignment = HorizontalAlignment.Center;


            TextBlock tblp = new TextBlock();
            tblp.Text = "Placa : " + placa;
            tblp.FontSize = 14;
            tblp.HorizontalAlignment = HorizontalAlignment.Center;
            tblp.VerticalAlignment = VerticalAlignment.Center;
            tblp.Foreground = new SolidColorBrush(Colors.White);
            tblp.FontWeight = FontWeights.Bold;
            brdt.Child = tblp;

            grtool.RowDefinitions.Add(new RowDefinition());
            grtool.Children.Add(brdt);
            Grid.SetRow(brdt, 0);

            Border brdt2 = new Border();
            brdt2.Background = new SolidColorBrush(Colors.White);
            brdt2.Width = 200;
            brdt2.Height = 25;
            brdt2.CornerRadius = new CornerRadius(5);
            brdt2.HorizontalAlignment = HorizontalAlignment.Center;

            tblp = new TextBlock();
            tblp.Text = "Estado : " + estado;
            tblp.FontSize = 11;
            tblp.HorizontalAlignment = HorizontalAlignment.Center;
            tblp.VerticalAlignment = VerticalAlignment.Center;
            tblp.TextAlignment = TextAlignment.Left;
            tblp.Foreground = new SolidColorBrush(Colors.Black);
            brdt2.Child = tblp;

            grtool.RowDefinitions.Add(new RowDefinition());
            grtool.Children.Add(brdt2);
            Grid.SetRow(brdt2, 1);

            brdt2 = new Border();
            brdt2.Background = new SolidColorBrush(Color.FromArgb(230, 247, 247, 182));
            brdt2.Width = 200;
            brdt2.Height = 200;
            brdt2.CornerRadius = new CornerRadius(5);
            brdt2.HorizontalAlignment = HorizontalAlignment.Center;

            TextBox tbx = new TextBox();
            tbx.TextWrapping = TextWrapping.Wrap;
            tbx.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            tbx.Height = 180;
            tbx.Width = 195;
            tbx.HorizontalAlignment = HorizontalAlignment.Center;
            tbx.VerticalAlignment = VerticalAlignment.Center;
            tbx.TextWrapping = TextWrapping.Wrap;
            tbx.Text = infoText;
            brdt2.Child = tbx;

            grtool.RowDefinitions.Add(new RowDefinition());
            grtool.Children.Add(brdt2);
            Grid.SetRow(brdt2, 2);




            ToolTipService.SetToolTip(pinpoint, grtool);



            MapLayer.SetLocation(pinpoint, new Location(latitud, longitud));




            ListaPuntosRuta.Add(pinpoint);
            dynamicLayer.Items.Add(pinpoint);

        }
        
        private ContentControl GetFormattedDescription(string description)
        {
            // format of description to control should be implemented
            // ...
            ContentControl control = new ContentControl();
            control.Content = description;

            return control;
        }
        
        #endregion
    }
}
