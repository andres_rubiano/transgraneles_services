﻿namespace Simplexity.Solla.Common.Constants
{
  public class MessagesConstants
  {
    public enum Types
    {
      None = 0,
      Information = 1,
      Warning = 2,
      Error = 3
    }
  }

  public class MessagesConstantsWrapper
  {
    private MessagesConstants.Types _t;
    public int Value
    {
      get
      {
        return (int)_t;
      }
      set
      {
        _t = (MessagesConstants.Types)value;
      }
    }
    public MessagesConstants.Types EnumValue
    {
      get
      {
        return _t;
      }
      set
      {
        _t = value;
      }
    }

    public static implicit operator MessagesConstantsWrapper(MessagesConstants.Types p)
    {
      return new MessagesConstantsWrapper { EnumValue = p };
    }

    public static implicit operator MessagesConstants.Types(MessagesConstantsWrapper pw)
    {
      return pw == null ? MessagesConstants.Types.Information : pw.EnumValue;
    }
  }
    public static class OperationConstants
    {
        public enum MessageSendStates : short
        {
            EnProcesamiento = 0,
            SinProcesar = 1,
            Procesado = 2,
            ProcesadoConErrores = 3
        }
    }
}
