﻿using System.Collections;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using Simplexity.Solla.Common.Properties;

namespace Simplexity.Solla.Common
{
    public static class Utility
    {

      /// <summary>
      /// Sends an email by using the XSLT template file. The template file is transform over a IDictionary object
      /// to an XHTML document. The contents in the subject line are replaced by the contents of title tag
      /// and the html body becomes the email contents.
      /// </summary>
      /// <param name="emailto">To email address</param>
      /// <param name="xslttemplatename">XSLT template file name</param>
      /// <param name="objDictionary">Dictonary objects containing data to be inserted in the transformed doc.</param>
    public  static string FormatEmail(IDictionary objDictionary)
      {

        byte[] byteArray = Encoding.ASCII.GetBytes(Resources.Template.ToCharArray());
        Stream xmlFile = new MemoryStream(byteArray);
        XmlReader xmlt = new XmlTextReader(xmlFile);
        XslTransform objxslt = new XslTransform();
        objxslt.Load(xmlt);

        XmlDocument xmldoc = new XmlDocument();
        xmldoc.AppendChild(xmldoc.CreateElement("DocumentRoot"));
        XPathNavigator xpathnav = xmldoc.CreateNavigator();


        XsltArgumentList xslarg = new XsltArgumentList();
        if (objDictionary != null)
          foreach (DictionaryEntry entry in objDictionary)
          {
            xslarg.AddExtensionObject(entry.Key.ToString(), entry.Value);
          }

        StringBuilder emailbuilder = new StringBuilder();
        XmlTextWriter xmlwriter = new XmlTextWriter(new System.IO.StringWriter(emailbuilder));

        objxslt.Transform(xpathnav, xslarg, xmlwriter, null);

        string subjecttext, bodytext;

        XmlDocument xemaildoc = new XmlDocument();
        xemaildoc.LoadXml(emailbuilder.ToString());
        XmlNode titlenode = xemaildoc.SelectSingleNode("//title");

        subjecttext = titlenode.InnerText;

        XmlNode bodynode = xemaildoc.SelectSingleNode("//body");

        bodytext = bodynode.InnerXml;
        if (bodytext.Length > 0)
        {
          bodytext = bodytext.Replace("&amp;", "&");
        }

        return bodytext;

      }

      /// <summary>
      /// Convierte una cadena a double. permite separador decimal , o . y retorna el de la configuracion regional
      /// </summary>
      /// <param name="strTextEntry">Cadena a convertir</param>
      /// <returns>double</returns>
      public static bool IsNumeric(string strTextEntry)
      {
        if (strTextEntry != string.Empty)
        {
          Regex objNotWholePattern = new Regex("[^0-9]");
          return !objNotWholePattern.IsMatch(strTextEntry);
        }
        return false;
      }

        public static double StringToDouble(string stringNumber)
          {
            double testNum = 0.5;
            char decimalSepparator;
            decimalSepparator = testNum.ToString()[1];
            return double.Parse(stringNumber.Replace('.', decimalSepparator).Replace(',', decimalSepparator));
          }
    }

    public class Receiver
    {
      private string _receiverEmailAddress;
 
      public Receiver()
      {

      }

      public string ReceiverEmailAddress
      {
        get
        {
          return _receiverEmailAddress;
        }
        set
        {
          _receiverEmailAddress = value;
        }
      }
      
    }

}
