﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace Services.Win.RunService
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    /// 
    //static void Main()
    //{
    //  ServiceBase[] ServicesToRun;
    //  ServicesToRun = new ServiceBase[] 
    //  { 
    //    new Service() 
    //  };
    //  ServiceBase.Run(ServicesToRun);
    //}

    static void Main()
    {
      #if DEBUG

            // Debug code: this allows the process to run as a non-service.
            // It will kick off the service start point, but never kill it.
            // Shut down the debugger to exit

            var mainService = new Service();
            mainService.StartService();

            ////Put a breakpoint on the following line to always catch your service when it has finished its work
            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);

      #else
                var servicesToRun = new ServiceBase[] 
                                                { 
                                                  new Service() 
                                                };
                ServiceBase.Run(servicesToRun);
      #endif
    }
  }
}
