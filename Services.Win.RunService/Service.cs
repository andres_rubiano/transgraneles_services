﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Configuration;
using System.Timers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;


namespace Services.Win.RunService
{
  public partial class Service : ServiceBase
  {
    private Timer _timer;
    
    public Service()
    {
      InitializeComponent();
    }

    protected override void OnStart(string[] args)
    {
      StartService();
    }

    public void StartService()
    {
      try
      {
        int interval;
        int.TryParse(ConfigurationManager.AppSettings["TimerInterval"], out interval);

        _timer = new Timer();
        _timer.Elapsed += TimerElapsed;
        _timer.Interval = interval;
        _timer.Enabled = true;

      }
      catch (Exception ex)
      {
        ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
      }
    }

    private void TimerElapsed(object sender, ElapsedEventArgs e)
    {
      var processName = ConfigurationManager.AppSettings["ProcessName"];
      var processPath = ConfigurationManager.AppSettings["ProcessPath"];
      
      _timer.Enabled = false;
      CheckProcessStatus(processName, processPath);
      _timer.Enabled = true;
    }

    protected override void OnStop()
    {
      Logger.Write(Resources.ReasonStopService, "General", 0, 778, TraceEventType.Information, "RunFaxServiceInfo");     
    }

    public void CheckProcessStatus(string processName, string processPath)
    {
      var getProcess = Process.GetProcessesByName(processName);
      try
      {
        if (getProcess.Length < 1)
        {
          Process.Start(processPath);
        }
      }
      catch (Exception ex)
      {
        ExceptionPolicy.HandleException(ex, "LoggingReplaceException");
      }
      //Si no hay ningun proceso ejecutandose lo levanta
    }

  }
}
