﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.Web.Services.Protocols;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Simplexity.Solla.Services.Wcf.Main.SollaCartera;

namespace Simplexity.Solla.Services.Wcf.Main
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
  //[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
  [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
  [ExceptionShielding("WCF Exception Shielding")]

  public class CarteraService : ICarteraService
  {
    public enum TipoMensaje
    {
      Information  = 1,
      Warning = 2,
      Error = 3
    }
    /// <summary>
    /// Retorna un string de la siguiente forma cartera|tipo de mensaje|mensaje
    /// </summary>
    /// <param name="nit"></param>
    /// <example>ejemplo de salida 1: 12234|1|
    /// ejemplo de salida 2: 0|2|no existe el cliente
    /// </example>
    /// <returns></returns>
    public string ConsultaCartera(string nit)
    {

      #region declararcion de variables

      var carteraServiceSettings = ConfigurationManager.GetSection("CarteraServiceSettings") as CarteraServiceSettings;

      string returnValue;

      #endregion

      #region verificar configuración

      if (carteraServiceSettings == null)
      {
        return string.Format("{0}|{1}|{2}", 0, (int)TipoMensaje.Warning, "Está ausente la sección de configuración");
      }

      if (String.IsNullOrEmpty(carteraServiceSettings.User) || String.IsNullOrEmpty(carteraServiceSettings.Password))
      {
        return string.Format("{0}|{1}|{2}", 0, (int)TipoMensaje.Warning, "El usuario o clave no están correctamente configurados");
      }
      if (String.IsNullOrEmpty(carteraServiceSettings.Sociedad))
      {
        return string.Format("{0}|{1}|{2}", 0, (int)TipoMensaje.Warning, "La sociedad no está correctamente configurada");
      }

      #endregion

      #region consumo de servicio

      using (var cartera = new ZWS_SALDO_CARTERA())
      {
        var networkCredential = new System.Net.NetworkCredential(carteraServiceSettings.User, carteraServiceSettings.Password);
        cartera.Credentials = networkCredential;

        try
        {
          decimal valorCartera;
          int tipoMensaje;
          string message = cartera.ZfiWsCartera(nit, carteraServiceSettings.Sociedad, out valorCartera, out tipoMensaje);
          returnValue = string.Format("{0}|{1}|{2}", valorCartera.ToString("0"), tipoMensaje, message);
        }
        catch (Exception ex)
        {
          returnValue = string.Format("{0}|{1}|{2}", "0", (int)TipoMensaje.Error, "ErrorWsSAP");
        }

      }

      #endregion

      return returnValue;
    }
  }
}
