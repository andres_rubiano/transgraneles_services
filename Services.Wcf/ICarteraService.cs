﻿using System.ServiceModel;
using Simplexity.Solla.Services.Wcf.Main.ErrorHandler;

namespace Simplexity.Solla.Services.Wcf.Main
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
  [ServiceContract, XmlSerializerFormat]
  public interface ICarteraService
  {

    [OperationContract]
    [FaultContract(typeof(ServiceFaultException))]
    string ConsultaCartera(string Nit);

  }

}
