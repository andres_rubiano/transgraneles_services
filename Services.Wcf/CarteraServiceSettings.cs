﻿using System.Configuration;
using System.Text;

namespace Simplexity.Solla.Services.Wcf.Main
{
  public class CarteraServiceSettings : ConfigurationSection
  {
    [ConfigurationProperty("User")]
    public string User
    {
      get { return (string)this["User"]; }
      set { this["User"] = value; }
    }

    [ConfigurationProperty("Password")]
    public string Password
    {
      get { return (string)this["Password"]; }
      set { this["Password"] = value; }
    }

    [ConfigurationProperty("Sociedad")]
    public string Sociedad
    {
      get { return (string)this["Sociedad"]; }
      set { this["Sociedad"] = value; }
    }

    public override string ToString()
    {
      var sb = new StringBuilder();
      sb.AppendFormat("Sociedad = {0}", Sociedad);

      return sb.ToString();
    }
  }
}