﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Xml.Linq;
using GenericWcfMessageInterceptor.Interfaces;
using Simplexity.Solla.BusinessRules.Main.Services;

namespace Simplexity.Solla.Services.Wcf.Main.SoapLogger
{
  public class Log : IServiceMessageInterceptor
  {
    private string _request;

    public void Receive(Message request, XDocument soapMessage)
    {
      if (!string.IsNullOrEmpty(request.ToString()))
      {
        _request = request.ToString();
      }
    }

    public void Respond(Message response, XDocument soapMessage)
    {
      if (!String.IsNullOrEmpty(_request))
      {
        var operationDescription = GetOperationDescription();
        var logservice = new LogService();

        logservice.CreateSoapCallLog(operationDescription.Name, _request, response.ToString());

      }
    }

    private OperationDescription GetOperationDescription()
    {
      OperationContext ctx = OperationContext.Current;
      ServiceDescription hostDesc = ctx.Host.Description;
      ServiceEndpoint endpoint = hostDesc.Endpoints.Find(ctx.IncomingMessageHeaders.To);
      string operationName =
        ctx.IncomingMessageHeaders.Action.Replace(endpoint.Contract.Namespace + endpoint.Contract.Name + "/", "");
      OperationDescription operation = endpoint.Contract.Operations.Find(operationName);

      return operation;
    }
  }
}