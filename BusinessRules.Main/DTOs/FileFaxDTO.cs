﻿namespace Simplexity.Solla.BusinessRules.Main.DTOs
{
  public class FileFaxDTO
  {
    public string OriginalFile { get; set; }
    public string ModifiedFile { get; set; }
    public string JobId { get; set; }
  }
}
