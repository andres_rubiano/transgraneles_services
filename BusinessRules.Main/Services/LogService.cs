﻿using System;
using Infrastructure.Data.Main.EF;

namespace Simplexity.Solla.BusinessRules.Main.Services
{
  public class LogService
  {
    public void CreateSoapCallLog(string operationName, string request, string response)
    {
      using (var gmEntities = new SollaLogEntities())
      {
        var soapCallLog = new SoapCallLog
        {
          OperationName = operationName,
          Request = request,
          Response = response,
          CallDateTime = DateTime.Now
        };

        gmEntities.AddToSoapCallLog(soapCallLog);
        gmEntities.SaveChanges();
      }
    }


  }
}
