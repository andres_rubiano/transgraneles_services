﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using FAXCOMEXLib;
using Infrastructure.Data.Main.EF;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Simplexity.Solla.BusinessRules.Main.DTOs;
using Simplexity.Solla.Common;
using Simplexity.Solla.Common.Constants;

namespace Simplexity.Solla.BusinessRules.Main.Services
{
    public class MessagesService : IDisposable
    {
        private static string _result;
        private readonly Email.Email _email;
        private Fax.Fax _fax;
        private const string NoValidNumber = "NVN";
        private const string FileNoFound = "FNF";
        private readonly int _maxMessageRetryAttemps;
        private readonly int _timeInSecondToRetryMessage;
        private List<Messages> _pendingEmailMessages;
        private List<Messages> _pendingFaxMessages;
        private readonly BooleanSwitch _traceSwitch;
        private List<FileFaxDTO> _filesFax;

        public MessagesService()
        {
            _traceSwitch = new BooleanSwitch("TraceSwitch", "Activa trazabilidad en el flujo del programa");

            #region Config Fax

            ConnectFax();

            #endregion

            #region Config Email
            _email = new Email.Email("EmailSettings");
            #endregion

            #region Set another config  parameters

            //MaxMessageRetryAttemps : aunque el fax hace un numero de reintentos, definimos que por regla nosotros vamos a hacer tres reintentos de reenvío del MENSAJE
            if (!int.TryParse(ConfigurationManager.AppSettings["MaxMessageRetryAttemps"], out _maxMessageRetryAttemps))
            {
                _maxMessageRetryAttemps = 3;
            }

            if (!int.TryParse(ConfigurationManager.AppSettings["TimeInSecondToRetryMessage"], out _timeInSecondToRetryMessage))
            {
                _timeInSecondToRetryMessage = 3;
            }
            #endregion
        }

        #region IDisposable Members

        public void Dispose()
        {
            _fax.FaxServer.Disconnect();
        }

        #endregion

        public void ProcessPendingMessages()
        {
            // 1. Get Pending email Messages
            _pendingEmailMessages = GetPendingEmailMessages();
            Console.WriteLine(@"{0} EMAIL - Registros a procesar {1}", DateTime.Now, _pendingEmailMessages.Count());
            InfoLog(Resources.Messages.logGetPendingEmailMessages);
            if (_pendingEmailMessages != null && _pendingEmailMessages.Count > 0)
            {
                SendEmailMessages();
                tryDelFileEmail();
                InfoLog(Resources.Messages.logSendEmailMessages);
            }
            // 2. Get Pending Fax Messages
            _filesFax = new List<FileFaxDTO>();
            _pendingFaxMessages = GetPendingFaxMessages();
            InfoLog(Resources.Messages.logGetPendingFaxMessages);
            if (_pendingFaxMessages != null && _pendingFaxMessages.Count > 0)
            {
                InfoLog(string.Format("{0} fax messages found to send", _pendingFaxMessages.Count));
                SendFaxMessages();
                InfoLog(Resources.Messages.logSendFaxMessages);
            }
        }

        #region private Methods
        private List<Messages> GetPendingFaxMessages()
        {
            var pendingMessages = new List<Messages>();

            using (var sollaEntities = new SollaEntities())
            {
                var faxMessages = (from p in sollaEntities.Messages
                                   where
                                     (p.MesFaxState == (byte)OperationConstants.MessageSendStates.SinProcesar ||
                                     p.MesFaxState == (byte)OperationConstants.MessageSendStates.EnProcesamiento) &&
                                     p.MesRetryAttempts <= _maxMessageRetryAttemps &&
                                     !string.IsNullOrEmpty(p.MesFaxNumber.Trim())
                                   select p).ToList();

                if (!faxMessages.Any())
                {
                    InfoLog(string.Format("No pending messages to process in this run"));
                    return pendingMessages;
                }

                var outgoingQueue = _fax.FaxServer.Folders.OutgoingQueue;
                if (outgoingQueue != null)
                {
                    outgoingQueue.Refresh();
                }

                var inQueue = outgoingQueue.GetJobs().Count;

                InfoLog(string.Format("Current pending faxes {0}", faxMessages.Count));
                InfoLog(string.Format("Current faxes in outgoing queue {0}", inQueue));

                foreach (var faxMessage in faxMessages)
                {
                    if (inQueue > 0 && !string.IsNullOrEmpty(faxMessage.MesFaxJobId))
                    {
                        // Try to find the job in the Queue and check its status 
                        var job = outgoingQueue.GetJob(faxMessage.MesFaxJobId);

                        if (job != null)
                        {
                            if (job.Status == FAX_JOB_STATUS_ENUM.fjsCANCELING)
                            {
                                string.Format("Message already in queue with the following info. MessageID {0}, JobId {1}, Status {2}, Extended Status {3}. Message will no be proccessed in this run", faxMessage.MesId, faxMessage.MesFaxJobId, Convert.ToString(job.Status), Convert.ToString(job.ExtendedStatusCode));
                                continue;
                            }

                            if (job.Status == FAX_JOB_STATUS_ENUM.fjsINPROGRESS || job.Status == FAX_JOB_STATUS_ENUM.fjsRETRYING)
                            {
                                if (job.ExtendedStatusCode == FAX_JOB_EXTENDED_STATUS_ENUM.fjesTRANSMITTING || job.ExtendedStatusCode == FAX_JOB_EXTENDED_STATUS_ENUM.fjesINITIALIZING || job.ExtendedStatusCode == FAX_JOB_EXTENDED_STATUS_ENUM.fjesDIALING)
                                {
                                    string.Format("Message already in queue with the following info. MessageID {0}, JobId {1}, Status {2}, Extended Status {3}. Message will no be proccessed in this run", faxMessage.MesId, faxMessage.MesFaxJobId, Convert.ToString(job.Status), Convert.ToString(job.ExtendedStatusCode));
                                    continue;
                                }
                            }
                        }

                    }

                    pendingMessages.Add(faxMessage);

                }

                InfoLog(string.Format("Total faxes to process in this run {0}", pendingMessages.Count));

                return pendingMessages;
            }
        }

        private List<Messages> GetPendingEmailMessages()
        {
            using (var sollaEntities = new SollaEntities())
            {
                IQueryable<Messages> query = (from p in sollaEntities.Messages
                                              where
                                                p.MesEmailState == (byte)OperationConstants.MessageSendStates.SinProcesar && p.MesProcessDate <= DateTime.Now
                                                && p.MesRetryAttempts <= _maxMessageRetryAttemps
                                              select p);
                return query.ToList();
            }
        }

        private void SendFaxMessages()
        {
            try
            {

                foreach (var message in _pendingFaxMessages)
                {
                    var messageDTO = CreateFaxNumber(message.MesFaxNumber);

                    if (messageDTO.TypeEnum != MessagesConstants.Types.Information)
                    {
                        message.MesMessage = messageDTO.Message;
                        message.MesFaxState = (byte)OperationConstants.MessageSendStates.ProcesadoConErrores;
                        message.MesFaxResult = NoValidNumber;
                        UpdateFaxMessage(message);

                        continue;
                    }

                    var faxNumber = messageDTO.TransactionNumber;

                    if (String.IsNullOrEmpty(message.MesFilePath))
                    {
                        message.MesMessage = Resources.Messages.messageProcessFilenotFound;
                        message.MesFaxState = (byte)OperationConstants.MessageSendStates.ProcesadoConErrores;
                        message.MesFaxResult = FileNoFound;
                        UpdateFaxMessage(message);
                        continue;
                    }

                    message.MesFaxState = (byte)OperationConstants.MessageSendStates.EnProcesamiento;
                    message.MesRetryAttempts++;

                    InfoLog(string.Format("Ready to send fax. Message Id {0}, Number {1}", message.MesId, faxNumber));

                    var messageDTOFax = _fax.SendFax(faxNumber, new Uri(message.MesFilePath), message.MesMessage,
                                                       ConfigurationManager.AppSettings["Faxsubject"]);


                    switch (messageDTOFax.TypeEnum)
                    {
                        case Fax.MessageDTO.Types.Information:

                            if (!string.IsNullOrEmpty(messageDTOFax.TransactionNumber))
                            {
                                message.MesFaxJobId = messageDTOFax.TransactionNumber;

                                _filesFax.Add(new FileFaxDTO() { JobId = message.MesFaxJobId, OriginalFile = _fax.FilePath, ModifiedFile = _fax.FileModifiedPath });

                                InfoLog(string.Format("SendFax processed sucessfully. JobId {0}, Originalfile {1}, ModifiedFile {2}. Total Files {3}", message.MesFaxJobId, _fax.FilePath, _fax.FileModifiedPath, _filesFax.Count));
                            }
                            else
                            {
                                ErrorLog(string.Format("SendFax does not return a jobid for Message Id {0}, Number {1}", message.MesId, faxNumber));

                                continue;
                            }

                            break;
                        case Fax.MessageDTO.Types.Error:
                        case Fax.MessageDTO.Types.Warning:

                            var messageFax = string.Format("Error from SendFax. Message Id {0}, Number {1}, Error {2}", message.MesId,
                                                        faxNumber, messageDTOFax.Message);

                            ErrorLog(messageFax);

                            break;
                    }


                    UpdateFaxMessage(message);
                }
            }

            finally
            {
                //_fax.Disconnect();
            }
        }

        private string GetBody()
        {
            return Utility.FormatEmail(null);
        }

        private void SendEmailMessages()
        {
            foreach (Messages message in _pendingEmailMessages)
            {
                if (!String.IsNullOrEmpty(message.MesFilePath)) // & File.Exists(message.MesFilePath)
                {
                    // Validar que message.MesEmail tenga el formato correcto:

                    if (ValidateEmail(message.MesEmail))
                    {
                        // Esto no es bonito pero aja:
                        _email.SendMailAttachment(message.MesEmail, ConfigurationManager.AppSettings["EmailPaymentSubject"], GetBody(), new Uri(message.MesFilePath));
                        InfoLog(String.Format(Resources.Messages.logSendMailAttachment, message.MesEmail,
                                              ConfigurationManager.AppSettings["EmailPaymentSubject"], GetBody(), message.MesFilePath));
                        if (_email.Response)
                        {
                            message.MesEmailState = (byte)OperationConstants.MessageSendStates.Procesado;
                            message.MesEmailResult = Resources.Messages.messageSendProcessEmailSucessful;
                            _email.Response = false;
                        }

                        else
                        {
                            // Reintentar pq el archivo aun no existe
                            if (message.MesRetryAttempts >= _maxMessageRetryAttemps)
                            {
                                message.MesEmailState = (byte)OperationConstants.MessageSendStates.ProcesadoConErrores;
                            }
                            else
                            {
                                message.MesRetryAttempts++;
                                message.MesEmailResult = string.Format(Resources.Messages.messageProcessFilenotFound, message.MesFilePath);
                                message.MesProcessDate = DateTime.Now.AddSeconds(_timeInSecondToRetryMessage);
                            }

                        }

                        //else
                        //{
                        //    message.MesEmailState = (byte)OperationConstants.MessageSendStates.ProcesadoConErrores;
                        //    message.MesEmailResult = string.Format(Resources.Messages.messageProcessFilenotFound, message.MesFilePath);
                        //}
                    }
                    else
                    {
                        message.MesEmailState = (byte)OperationConstants.MessageSendStates.ProcesadoConErrores;
                        message.MesEmailResult = string.Format(Resources.Messages.messageInvalidEmail, message.MesEmail);
                    }
                }
                else
                {
                    message.MesEmailState = (byte)OperationConstants.MessageSendStates.ProcesadoConErrores;
                    message.MesEmailResult = string.Format(Resources.Messages.messageProcessFilenotFound, message.MesFilePath);
                }

                message.MesProcessedDate = DateTime.Now;
                UpdateEmailMessage(message);
                InfoLog(String.Format(Resources.Messages.logUpdateEmailMessage, message));
            }
        }

        private void UpdateEmailMessage(Messages messageCandidate)
        {
            using (var sollaEntities = new SollaEntities())
            {
                var message = (from p in sollaEntities.Messages
                               where p.MesId == messageCandidate.MesId
                               select p).FirstOrDefault();

                if (message != null)
                {
                    message.MesEmailState = messageCandidate.MesEmailState;
                    message.MesEmailResult = messageCandidate.MesEmailResult;
                    message.MesRetryAttempts = messageCandidate.MesRetryAttempts;
                    message.MesProcessDate = messageCandidate.MesProcessDate;
                    message.MesProcessedDate = messageCandidate.MesProcessedDate;
                }

                sollaEntities.SaveChanges();
            }
        }

        private void UpdateFaxMessage(Messages messageCandidate)
        {
            using (var sollaEntities = new SollaEntities())
            {
                var message = (from p in sollaEntities.Messages
                               where p.MesId == messageCandidate.MesId
                               select p).FirstOrDefault();

                if (message != null)
                {
                    message.MesFaxState = messageCandidate.MesFaxState;
                    message.MesFaxResult = messageCandidate.MesFaxResult;
                    message.MesMessage = messageCandidate.MesMessage;
                    message.MesFaxJobId = messageCandidate.MesFaxJobId;
                    message.MesRetryAttempts = messageCandidate.MesRetryAttempts;
                }

                sollaEntities.SaveChanges();
            }
        }

        public MessageDTO CreateFaxNumber(string rawFaxNumber)
        {
            var message = new MessageDTO();

            //rawFaxNumber para este servicio llega <indicativo>-<numero>
            //Debe armar un numeor con las siguientes caracteristicas:
            //<numero de desbloqueo><operador><indicativo><numero>
            //<numero de desbloqueo>: es el numero para desbloquear el telefono y obtener TONO de marcado: parametro configurable
            //<operador> 07 es ETB, 09 es Telefonica, 05 UNE
            //<indicativo>: indicativo de la ciudad
            //<numero>
            string operatorCode = ConfigurationManager.AppSettings["OperatorCode"];
            string clientAreaCode = ConfigurationManager.AppSettings["ClientAreaCode"];
            string unlockNum = ConfigurationManager.AppSettings["UnlockNum"];
            string validOperatorCodes = ConfigurationManager.AppSettings["ValidOperatorCodes"];

            string indicativoFax = string.Empty;
            string numeroFax = string.Empty;
            string newFaxnumber = string.Empty;
            string extension = string.Empty;
            var pauses = 0;

            #region validate raw number

            // Validate rawnumber. Valid Structure is (extension is opcional)
            // <area code>-<number>,<extension>
            //    <area code>: 1 digit.
            //    <number>: 7 digits.
            //    <extension>: 7 digits max.
            //  Valid Samples
            //    1-6742237 (bogotá)
            //    4-235548 (medellin)
            //    1-6742237,2045 (bogotá with extension)
            //    4-235548,34554 (medellin with extension)
            //  Invalid Samples
            //    asaasasa (No Numbers)
            //    6742237  (no area code)

            // 1. Is Empty?
            if (String.IsNullOrEmpty(rawFaxNumber))
            {
                message.TypeEnum = MessagesConstants.Types.Error;
                message.Message = string.Format("El numero es nulo o vacio");
                return message;
            }

            var tempnumber = rawFaxNumber.Split('-');

            // 2. Must have area code
            if (tempnumber.Length != 2)
            {
                message.TypeEnum = MessagesConstants.Types.Error;
                message.Message = string.Format("El numero ({0}) debe contener un código de area, eje 1-6742237 (Bogotá)",
                                                rawFaxNumber);
                return message;
            }

            indicativoFax = tempnumber[0];

            // 3. Area Code must be 1 digit.
            if (indicativoFax.Length != 1)
            {
                message.TypeEnum = MessagesConstants.Types.Error;
                message.Message = string.Format("El código de área ({0}) debe ser de un solo dígito", indicativoFax);
                return message;
            }

            var tempnumberextension = tempnumber[1].Split(',');

            // 4. get fax number and/or extension
            if (tempnumberextension.Length >= 2)
            {
                numeroFax = tempnumberextension[0];
                extension = tempnumberextension[tempnumberextension.Length - 1];
                pauses = tempnumberextension.Length - 1;

                if (string.IsNullOrEmpty(extension))
                {
                    message.TypeEnum = MessagesConstants.Types.Error;
                    message.Message = string.Format("El número de extensión no es valido");
                    return message;
                }
            }
            else
            {
                numeroFax = tempnumber[1];
            }

            if (!Utility.IsNumeric(numeroFax))
            {
                message.TypeEnum = MessagesConstants.Types.Error;
                message.Message = string.Format("La cadena ({0}) no es un número válido de fax", numeroFax);
                return message;
            }

            // 5. Number must be 7 digits
            if (numeroFax.Length != 7)
            {
                message.TypeEnum = MessagesConstants.Types.Error;
                message.Message = string.Format("El número ({0}) debe ser de 7 dígitos", numeroFax);
                return message;
            }

            // 6. Check extension if any
            if (!string.IsNullOrEmpty(extension) && !Utility.IsNumeric(extension))
            {
                message.TypeEnum = MessagesConstants.Types.Error;
                message.Message = string.Format("El número ({0}) de extensión no es valido", extension);
                return message;
            }

            #endregion

            if (!String.IsNullOrEmpty(unlockNum) && !Utility.IsNumeric(unlockNum))
            {
                message.TypeEnum = MessagesConstants.Types.Error;
                message.Message = string.Format("El numero de desbloqueo ({0}) no es valido. Debe ser un número", unlockNum);
                return message;
            }

            if (!String.IsNullOrEmpty(operatorCode) && !validOperatorCodes.Contains(operatorCode))
            {
                message.TypeEnum = MessagesConstants.Types.Error;
                message.Message = string.Format("El número del operador ({0}) no es valido. Numeros validos son ({1})", operatorCode, validOperatorCodes);
                return message;
            }

            if (!String.IsNullOrEmpty(clientAreaCode) && clientAreaCode.Length > 1 && !Utility.IsNumeric(clientAreaCode))
            {
                message.TypeEnum = MessagesConstants.Types.Error;
                message.Message = string.Format("El código de área configurado para el cliente ({0}) no es valido. Debe ser un numero de 1 dígito entre 1 y 9", clientAreaCode);
                return message;
            }


            newFaxnumber = clientAreaCode == indicativoFax
              ? string.Format("{0}{1}{2}", unlockNum, numeroFax, extension.Length > 0 ? new String(',', pauses) + extension : extension)
                                : string.Format("{0}{1}{2}{3}{4}", unlockNum, operatorCode, indicativoFax, numeroFax, extension.Length > 0 ? new String(',', pauses) + extension : extension);

            message.TransactionNumber = newFaxnumber;

            return message;
        }

        private void RemoveJob(string severJobID)
        {
            FaxOutgoingQueue outgoingQueues;
            IFaxOutgoingJob outgoingJobs;
            outgoingQueues = _fax.FaxServer.Folders.OutgoingQueue;
            outgoingQueues.Refresh();
            outgoingJobs = outgoingQueues.GetJob(severJobID);
            outgoingJobs.Cancel();
        }

        private void InfoLog(string info)
        {
            if (_traceSwitch.Enabled)
            {
                Logger.Write(info, "General", 0, 111, TraceEventType.Information, "FaxSenderInfo");
            }

        }

        private void ErrorLog(string info)
        {
            if (_traceSwitch.Enabled)
            {
                Logger.Write(info, "General", 0, 999, TraceEventType.Error, "FaxSenderInfo");
            }

        }


        private void tryDelFileEmail()
        {
            while (!DeleteFileEmail())
            {
                DeleteFileEmail();
            }
        }

        private bool DeleteFileEmail()
        {
            if (File.Exists(_email.FileName))
            {
                try
                {
                    File.Delete(_email.FileName);
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            return true;
        }

        private bool ValidateEmail(string email)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(email);
            return match.Success;
        }
        #endregion

        #region Fax Events
        //This event receives the FaxJobStatus object
        public void objFaxServer_OnOutgoingJobChanged(FaxServer pFaxServer, string bstrJobId, FaxJobStatus pJobStatus)
        {

            var deleteFiles = false;
            string messageFax = string.Empty;

            var message = _pendingFaxMessages.FirstOrDefault(p => p.MesFaxJobId == bstrJobId);

            if (message == null) return;

            _result = (Convert.ToString(pJobStatus.Status));
            message.MesFaxResult = _result;
            messageFax = string.Format("MessageID {0}, JobId {1}, Status {2}, Extended Status {3}, Retries {4}", message.MesId, message.MesFaxJobId, _result,
                                               Convert.ToString(pJobStatus.ExtendedStatusCode), message.MesRetryAttempts);
            InfoLog(string.Format("OnOutgoingJobChanged {0}", messageFax));

            switch (pJobStatus.Status)
            {
                case FAX_JOB_STATUS_ENUM.fjsCOMPLETED:
                    message.MesFaxState = (byte)OperationConstants.MessageSendStates.Procesado;
                    message.MesMessage = null;
                    InfoLog(messageFax);
                    deleteFiles = true;
                    break;

                case FAX_JOB_STATUS_ENUM.fjsINPROGRESS:
                case FAX_JOB_STATUS_ENUM.fjsNOLINE:
                case FAX_JOB_STATUS_ENUM.fjsRETRYING:
                case FAX_JOB_STATUS_ENUM.fjsPENDING:
                case FAX_JOB_STATUS_ENUM.fjsRETRIES_EXCEEDED:
                    message.MesMessage = messageFax;

                    deleteFiles = message.MesRetryAttempts > _maxMessageRetryAttemps;

                    break;

                case FAX_JOB_STATUS_ENUM.fjsFAILED:
                    message.MesFaxState = (byte)OperationConstants.MessageSendStates.ProcesadoConErrores;
                    message.MesMessage = messageFax;
                    deleteFiles = true;
                    break;

                default:
                    message.MesFaxState = (byte)OperationConstants.MessageSendStates.ProcesadoConErrores;
                    message.MesMessage = messageFax;
                    deleteFiles = true;
                    break;
            }

            UpdateFaxMessage(message);

            if (!deleteFiles) return;
            if (!_filesFax.Any()) return;

            InfoLog(String.Format("Files in queue to delete:{0}", _filesFax.Count));

            var file = _filesFax.FirstOrDefault(p => p.JobId == bstrJobId);

            if (file == null) return;

            InfoLog(String.Format("Ready to delete files Original ({0}), Modified ({1})", file.OriginalFile, file.ModifiedFile));

            if (File.Exists(file.OriginalFile))
            {
                File.Delete(file.OriginalFile);
                InfoLog(String.Format("File Original deleted ({0})", file.OriginalFile));
            }

            if (File.Exists(file.ModifiedFile))
            {
                File.Delete(file.ModifiedFile);
                InfoLog(String.Format("File Modified deleted ({0})", file.ModifiedFile));
            }
        }

        //Should be raised when fax is sent and is currently not...
        private void objFaxServer_OnServerShutDown(FaxServer pFaxServer)
        {
            _result = "Server shoutdown";

            foreach (var message in _pendingFaxMessages)
            {
                message.MesFaxState = (byte)OperationConstants.MessageSendStates.ProcesadoConErrores;
                message.MesFaxResult = _result;
                UpdateFaxMessage(message);
                InfoLog(String.Format(Resources.Messages.logUpdateFaxMessage, message));
            }
        }

        private void ConnectFax()
        {
            _fax = new Fax.Fax();

            InfoLog("Fax instance set correctly");

            // 1. Configure los eventos a escuchar
            _fax.FaxServer.OnOutgoingJobChanged += objFaxServer_OnOutgoingJobChanged;
            _fax.FaxServer.OnServerShutDown += objFaxServer_OnServerShutDown;

            InfoLog("Fax Server events set correctly. OnOutgoingJobChanged, OnServerShutDown");

            // 1. Connnect to Fax if pending messages are > 0
            _fax.Connect();
            InfoLog(string.Format("Fax Server connected correctly. Connected status {0}, Retries {1}, RetryDelay {2}", _fax.Connected, _fax.FaxServer.Configuration.Retries, _fax.FaxServer.Configuration.RetryDelay));

            //fsetFXSSVC_ENDED: The client requests notifications when the fax service stops executing.
            _fax.FaxServer.ListenToServerEvents(FAX_SERVER_EVENTS_TYPE_ENUM.fsetFXSSVC_ENDED);
            //fsetOUT_QUEUE: The client requests notification about fax jobs in the outgoing queue. When the status of an outgoing fax job changes, 
            //the fax service issues a notification of this type.
            _fax.FaxServer.ListenToServerEvents(FAX_SERVER_EVENTS_TYPE_ENUM.fsetOUT_QUEUE);

            InfoLog(string.Format("Fax Server listening for events {0}, {1}", "fsetFXSSVC_ENDED", "fsetOUT_QUEUE"));
        }


        #endregion
    }
}