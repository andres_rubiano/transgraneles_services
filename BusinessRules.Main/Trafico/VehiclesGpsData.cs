﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.Solla.BusinessRules.Main.Trafico
{
    public class VehiclesGpsData
    {
        public string VehCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Direction { get; set; }
        public double Speed { get; set; }
        public string GeoRef { get; set; }
        public string GpsDate { get; set; }
        public int Deltac { get; set; }
    }
}
